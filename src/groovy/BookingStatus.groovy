
/**
 * Created with IntelliJ IDEA.
 * User: damien
 * Date: 23/09/12
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
public enum BookingStatus {

    BOOKED ('Booked'),
    CONFIRMED ('Confirmed'),
    PAYMENT_REFUSED ('Payment Refused'),
    CANCELLED_BY_USER ('Cancelled by User'),
    CANCELLED_BY_ADMIN ('Cancelled by Admin')
}