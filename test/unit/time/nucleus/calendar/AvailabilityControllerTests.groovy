package time.nucleus.calendar



import org.junit.*
import grails.test.mixin.*

@TestFor(AvailabilityController)
@Mock(Availability)
class AvailabilityControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/availability/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.availabilityInstanceList.size() == 0
        assert model.availabilityInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.availabilityInstance != null
    }

    void testSave() {
        controller.save()

        assert model.availabilityInstance != null
        assert view == '/availability/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/availability/show/1'
        assert controller.flash.message != null
        assert Availability.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/availability/list'

        populateValidParams(params)
        def availability = new Availability(params)

        assert availability.save() != null

        params.id = availability.id

        def model = controller.show()

        assert model.availabilityInstance == availability
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/availability/list'

        populateValidParams(params)
        def availability = new Availability(params)

        assert availability.save() != null

        params.id = availability.id

        def model = controller.edit()

        assert model.availabilityInstance == availability
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/availability/list'

        response.reset()

        populateValidParams(params)
        def availability = new Availability(params)

        assert availability.save() != null

        // test invalid parameters in update
        params.id = availability.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/availability/edit"
        assert model.availabilityInstance != null

        availability.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/availability/show/$availability.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        availability.clearErrors()

        populateValidParams(params)
        params.id = availability.id
        params.version = -1
        controller.update()

        assert view == "/availability/edit"
        assert model.availabilityInstance != null
        assert model.availabilityInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/availability/list'

        response.reset()

        populateValidParams(params)
        def availability = new Availability(params)

        assert availability.save() != null
        assert Availability.count() == 1

        params.id = availability.id

        controller.delete()

        assert Availability.count() == 0
        assert Availability.get(availability.id) == null
        assert response.redirectedUrl == '/availability/list'
    }
}
