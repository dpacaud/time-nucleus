package time.nucleus



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AvailabilityService)
class AvailabilityServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
