package time.nucleus



import grails.test.mixin.*
import time.nucleus.calendar.Availability

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Availability)
class AvailabilityTests {

    void testSomething() {
        fail "Implement me"
    }
}
