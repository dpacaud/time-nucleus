package time.nucleus



import grails.test.mixin.*

import time.nucleus.hotels.Hotel

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Hotel)
class HotelTests {

    void testSomething() {
        fail "Implement me"
    }
}
