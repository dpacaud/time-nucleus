package time.nucleus.booking



import org.junit.*
import grails.test.mixin.*

@TestFor(BookingController)
@Mock(Booking)
class BookingControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/booking/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.bookingInstanceList.size() == 0
        assert model.bookingInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.bookingInstance != null
    }

    void testSave() {
        controller.save()

        assert model.bookingInstance != null
        assert view == '/booking/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/booking/show/1'
        assert controller.flash.message != null
        assert Booking.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/booking/list'

        populateValidParams(params)
        def booking = new Booking(params)

        assert booking.save() != null

        params.id = booking.id

        def model = controller.show()

        assert model.bookingInstance == booking
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/booking/list'

        populateValidParams(params)
        def booking = new Booking(params)

        assert booking.save() != null

        params.id = booking.id

        def model = controller.edit()

        assert model.bookingInstance == booking
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/booking/list'

        response.reset()

        populateValidParams(params)
        def booking = new Booking(params)

        assert booking.save() != null

        // test invalid parameters in update
        params.id = booking.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/booking/edit"
        assert model.bookingInstance != null

        booking.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/booking/show/$booking.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        booking.clearErrors()

        populateValidParams(params)
        params.id = booking.id
        params.version = -1
        controller.update()

        assert view == "/booking/edit"
        assert model.bookingInstance != null
        assert model.bookingInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/booking/list'

        response.reset()

        populateValidParams(params)
        def booking = new Booking(params)

        assert booking.save() != null
        assert Booking.count() == 1

        params.id = booking.id

        controller.delete()

        assert Booking.count() == 0
        assert Booking.get(booking.id) == null
        assert response.redirectedUrl == '/booking/list'
    }
}
