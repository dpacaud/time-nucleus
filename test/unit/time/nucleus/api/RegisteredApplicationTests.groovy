package time.nucleus.api



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(RegisteredApplication)
class RegisteredApplicationTests {

    void testSomething() {
       fail "Implement me"
    }
}
