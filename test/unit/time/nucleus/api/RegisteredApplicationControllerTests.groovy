package time.nucleus.api



import org.junit.*
import grails.test.mixin.*

@TestFor(RegisteredApplicationController)
@Mock(RegisteredApplication)
class RegisteredApplicationControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/registeredApplication/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.registeredApplicationInstanceList.size() == 0
        assert model.registeredApplicationInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.registeredApplicationInstance != null
    }

    void testSave() {
        controller.save()

        assert model.registeredApplicationInstance != null
        assert view == '/registeredApplication/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/registeredApplication/show/1'
        assert controller.flash.message != null
        assert RegisteredApplication.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/registeredApplication/list'

        populateValidParams(params)
        def registeredApplication = new RegisteredApplication(params)

        assert registeredApplication.save() != null

        params.id = registeredApplication.id

        def model = controller.show()

        assert model.registeredApplicationInstance == registeredApplication
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/registeredApplication/list'

        populateValidParams(params)
        def registeredApplication = new RegisteredApplication(params)

        assert registeredApplication.save() != null

        params.id = registeredApplication.id

        def model = controller.edit()

        assert model.registeredApplicationInstance == registeredApplication
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/registeredApplication/list'

        response.reset()

        populateValidParams(params)
        def registeredApplication = new RegisteredApplication(params)

        assert registeredApplication.save() != null

        // test invalid parameters in update
        params.id = registeredApplication.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/registeredApplication/edit"
        assert model.registeredApplicationInstance != null

        registeredApplication.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/registeredApplication/show/$registeredApplication.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        registeredApplication.clearErrors()

        populateValidParams(params)
        params.id = registeredApplication.id
        params.version = -1
        controller.update()

        assert view == "/registeredApplication/edit"
        assert model.registeredApplicationInstance != null
        assert model.registeredApplicationInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/registeredApplication/list'

        response.reset()

        populateValidParams(params)
        def registeredApplication = new RegisteredApplication(params)

        assert registeredApplication.save() != null
        assert RegisteredApplication.count() == 1

        params.id = registeredApplication.id

        controller.delete()

        assert RegisteredApplication.count() == 0
        assert RegisteredApplication.get(registeredApplication.id) == null
        assert response.redirectedUrl == '/registeredApplication/list'
    }
}
