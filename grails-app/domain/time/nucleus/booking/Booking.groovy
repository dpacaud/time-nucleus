package time.nucleus.booking

import time.nucleus.hotels.Room
import time.nucleus.hotels.Chain

class Booking {

    static belongsTo = [room:Room]

    Date dateCreated
    Date lastUpdated
    Chain chain

    static constraints = {
    }
}
