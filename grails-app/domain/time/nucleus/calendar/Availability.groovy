package time.nucleus.calendar

import time.nucleus.hotels.Room

class Availability {

    Date startTime

    Date endTime

    // Ces variables représentent les heures de disponibilités de la chambre
//    int startHour
//    int startMin
//    int endHour
//    int endMin

    // A quelle heure les pax doivent-ils partir ?
    int leavingHour
    int leavingMin

    // This integer represents the amount of available Objects for this particular timeSpan
    int total

    BigDecimal basePrice

    BigDecimal reducedPrice

    static belongsTo = [room:Room]

    Date dateCreated
    Date lastUpdated

    static constraints = {
//        startHour   nullable:true
//        startMin    nullable:true
//        endHour     nullable:true
//        endMin      nullable:true
        leavingHour nullable:true
        leavingMin  nullable:true
    }
}
