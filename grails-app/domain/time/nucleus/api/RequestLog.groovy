package time.nucleus.api

class RequestLog {

    String apiMethodName
    String apiMethodParams
    String requestUrl

    Date dateCreated
    Date lastUpdated

    static belongsTo = [application: RegisteredApplication]

    static constraints = {
        apiMethodName nullable: false
        apiMethodParams nullable: true
        requestUrl nullable: true
    }

    static mapping = {
        version false
    }

}
