package time.nucleus.api

/**
 * This class defines the websites that will have access to the Time-Nucleus API
 *
 */

class RegisteredApplication {

    String name
    String apiKey = generateRandomApiKey()

    boolean active

    Date dateCreated
    Date lastUpdated

    String authorizedMethods = null

    static hasMany = [requestLogs: RequestLog]

    static constraints = {
        name blank: false
        apiKey nullable: false, unique: true
        authorizedMethods nullable: true
    }

    static mapping = {
        version false
    }


    /**
     *
     * @return
     */
    def generateRandomApiKey() {

        String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&#@+-=!"
        int API_KEY_SIZE = 32

        String apiKey = "";

        def random = new Random()
        def randomPos

        int nbAvailableChars = CHARACTERS.length()

        32.times() {
            randomPos = random.nextInt(nbAvailableChars)
            apiKey += CHARACTERS[randomPos]
        }

        return apiKey
    }

}
