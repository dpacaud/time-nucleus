package time.nucleus.security

import time.nucleus.hotels.Hotel

class User {

	transient springSecurityService

	String username
    String lastName
    String firstName
    String email

	String password
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

    Date dateCreated
    Date lastUpdated


    static hasMany = [hotels : Hotel]

	static constraints = {
		username blank: false, unique: true
		password blank: false
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

    List<Hotel>  getHotelsNotManagedByUser() {
        return Hotel.list() - this.hotels
    }
}
