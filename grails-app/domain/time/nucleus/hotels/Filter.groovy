package time.nucleus.hotels

class Filter {

    String name

    String description

    static hasMany = [hotels:Hotel]
    static belongsTo = Hotel

    static constraints = {
    }
}
