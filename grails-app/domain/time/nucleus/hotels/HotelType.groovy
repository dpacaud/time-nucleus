package time.nucleus.hotels

class HotelType {

    String name

    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    String toString(){
        return this.name
    }
    static toJson = {
        def returnMap = [:]
        returnMap["id"] = it.id
        returnMap["name"] = it.name
        returnMap
    }
}
