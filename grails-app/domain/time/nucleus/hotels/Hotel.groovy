package time.nucleus.hotels

import time.nucleus.security.User

class Hotel {

    String name

    String description

    int stars

    Chain chain

    HotelType type

    /**********************************
     *   Contact information
     **********************************/

    String adressL1
    String adressL2

    // A voir si nous avons interêt à sortir ces éléments dans un autre objet
    String city
    String zipCode

    String telephoneFixe
    String telephoneMobile
    String fax
    String contactEmail
    String website

    // Does the Hotel allow cash payment ?
    boolean cashAccepted = false

    boolean isPublished = false

    /** Hotel legal information **/
    String raisonSociale
    String siret
    String tvaNumber

    //Coordonnées GPS pour la géolocalisation
    double lat
    double lng

    double commissionRate

    static hasMany = [rooms:Room, filters:Filter, users:User, photos:Photo]

    static belongsTo = [User]

    Date dateCreated
    Date lastUpdated

    static constraints = {
        name blank: false
        chain (nullable: true)
        lat nullable:true
        lng nullable:true
        raisonSociale nullable:true
        siret nullable:true
        tvaNumber nullable:true
        description maxSize: 1000, nullable: true
        adressL2 nullable: true
        adressL1 blank: false
        stars min:0, max: 5
        telephoneFixe nullable: true , matches: /^0[1-689][0-9]{8}$/
        fax nullable: true , matches: /^0[1-689][0-9]{8}$/
        telephoneMobile  nullable: false, blank: false, matches: /^06[0-9]{8}$/
        website nullable: true
        filters nullable: true
    }
    static mapping = {
        version false
    }

    def transient getCompleteAddress(){
      return [this.adressL1, this.adressL2 ? this.adressL2 : "", this.zipCode, this.city].join(" ")
    }

    def transient getRoomCount(){
        return this.rooms ? this.rooms.size() : 0
    }

    static toJson = {
        def returnMap = [:]
        returnMap["id"] = it.id
        returnMap["type"] = it.type.toString()
        returnMap["name"] = it.name
        returnMap["description"] = it.description
        returnMap["adressL1"] = it.adressL1
        returnMap["adressL2"] = it.adressL2
        returnMap["zipCode"] = it.zipCode
        returnMap["city"] = it.city
        returnMap["lat"] = it.lat
        returnMap["lng"] = it.lng
        returnMap["website"] = it.website
        returnMap["cashAccepted"] = it.cashAccepted
        returnMap["stars"] = it.stars
        returnMap["photos"] = it.photos
        returnMap
    }
}
