package time.nucleus.hotels

class Chain {

    String name

    static hasMany = [hotels:Hotel]

    Date dateCreated
    Date lastUpdated

    static constraints = {

    }
}
