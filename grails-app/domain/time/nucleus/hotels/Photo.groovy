package time.nucleus.hotels

import org.apache.commons.logging.LogFactory

class Photo {

    private static final log = LogFactory.getLog(this)

    String originalFileName
    String computedFileName
   // String hashKey
    String mimeType
    String filePath
    String relativePath


    static constraints = {
        originalFileName blank: false
        mimeType blank: false
        filePath nullable: true
    }

    static mapping = {
        version false
    }

    static transient Photo create(uploadedFile, obj, relativePath, computedName) {

        //def test = grailsApplication.config

        Photo p = new Photo(
                originalFileName: uploadedFile.originalFilename,
                computedFileName : computedName,
                mimeType: uploadedFile.contentType,
                filePath: getGrailsApplication().config.timeNucleus.photos.rootPath + File.separator + relativePath,
                relativePath: relativePath
        )


        if(!p.save()) {

            log.error "Unable to save photo to database: [filename: ${p?.originalFileName}, mimeType: ${p?.mimeType}]"
            log.error "See errors below"
            p.errors.each {
                log.error it
            }

            return null
        }
        else {
            // On ajoute la photo à l'objet Room ou Hotel
            obj.addToPhotos(p)
        }
        log.info "New photo saved to database: [filename: ${p?.originalFileName}, mimeType: ${p?.mimeType}]"
        return p
    }


    static toJson = {
        def returnMap = [:]
        returnMap["id"] = it.id
        returnMap["relativePath"] = it.relativePath
        returnMap["mimeType"] = it.mimeType
        returnMap
    }

}
