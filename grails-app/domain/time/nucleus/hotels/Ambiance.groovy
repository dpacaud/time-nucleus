package time.nucleus.hotels

class Ambiance {

    String name
    String description

    static hasMany = [rooms:Room]
    static constraints = {
        description nullable: true
    }

    static mapping = {
        version false
    }
}
