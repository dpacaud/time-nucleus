package time.nucleus.hotels

import time.nucleus.calendar.Availability
import time.nucleus.booking.Booking

class Room {
    def photoService


    String name
    String description

    // Nombre de pax maximum pour cette chambre
    int nbAcceptedPax = 2

    static belongsTo = [hotel:Hotel]
    static hasOne = [ambiance: Ambiance]
    static hasMany = [availabilities:Availability, bookings:Booking, photos:Photo]

    Date dateCreated
    Date lastUpdated

    static constraints = {
        description maxSize: 1000, nullable: true
        ambiance nullable: true
    }

    static mapping = {
        version false
    }

    def beforeDelete() {
        Room.withNewSession {
            def path = getGrailsApplication().config.timeNucleus.photos.rootPath + File.separator + photoService.getFolderRelativePath(this)
            log.info "A room is being deleted"
            log.info "We need to clean it's photo directory"
            log.info "Deleting Dir : [${path}]"
            def file = new File(path)
            if (file.deleteDir()){
                log.info("Directory has been deleted")
            }
            else {
                log.error("Directory [${path}] could not be deleted")
            }
        }
    }
}
