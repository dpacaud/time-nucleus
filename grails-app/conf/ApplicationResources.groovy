modules = {
    application {
        dependsOn 'jqueryUI,jquerySettings,toggle,tipsy,uniform'
        resource url:'js/application.js'
    }

    jqueryBase {
        resource url:'http://code.jquery.com/jquery-1.8.2.js'
        //TODO : find a better place to put this css
        resource url:'css/iphone-style-switches.css',disposition: 'head'
        resource url:'css/root.css',disposition: 'head'
        resource url:'css/grid.css',disposition: 'head'
        resource url:'css/typography.css',disposition: 'head'
    }


    jqueryUI {
        dependsOn 'jqueryBase'
        resource url:'http://code.jquery.com/ui/1.9.1/jquery-ui.js'
        //resource url:'/js/jquery.ui.core.js'
        //resource url:'/js/jquery.ui.mouse.js'
        //resource url:'/js/jquery.ui.widget.js'
        resource url:'http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css', disposition: 'head'
        //resource url:'/css/jquery.ui/jquery.ui.base.css',disposition: 'head'
        resource url:'/css/jquery.ui/jquery.ui.theme.css',disposition: 'head'
    }

    jquerySettings {
        dependsOn 'jqueryBase,wysiwyg, tableShorter'
        resource url:'js/jquery-settings.js'
    }

    toggle {
        dependsOn 'jqueryBase'
        resource url:'js/toogle.js'
    }

    tipsy {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.tipsy.js'
        resource url:'css/tipsy.css'
    }

    uniform {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.uniform.min.js'
        resource url:'css/uniform.default.css',disposition: 'head'
    }

    wysiwyg {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.wysiwyg.js'
        resource url:'css/jquery.wysiwyg.css',disposition: 'head'
    }

    tableShorter {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.tablesorter.min.js'
        resource url:'css/tableshorter-theme.css',disposition: 'head'
    }

    raphael {
        resource url:'js/raphael.js'

    }

    raphaelAnalytics {
        dependsOn('raphael')
        resource url:'js/analytics.js'
        resource url:'js/popup.js'
    }

    fullCalendar {
        dependsOn 'jqueryBase'
        resource url:'js/fullcalendar.js'
        resource url:'css/fullcalendar.css',disposition: 'head'
    }

    prettyPhoto {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.prettyPhoto.js'
        resource url:'css/prettyphoto-style.css',disposition: 'head'
    }

    slider {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.ui.slider.js'
    }

    datePicker {
        dependsOn 'jqueryBase,jqueryUI'
        resource url:'js/jquery.ui.datepicker.js'
    }

    tabs {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.ui.tabs.js'
    }

    accordion {
        dependsOn 'jqueryBase'
        resource url:'js/jquery.ui.accordion.js'
    }
 //   googleJSAPI {
 //        resource url:'https://www.google.time.nucleus/jsapi'
 //   }
    dataTables{
        dependsOn 'jqueryBase'
        resource url:'js/jquery.dataTables.js'
        resource url:'css/datatable_jui.css',disposition: 'head'
    }

    hotels {
        dependsOn 'jqueryBase, jquerySettings'
        resource url:'js/hotel.js'
    }

    room {
        dependsOn 'jqueryBase'
        resource url:'js/room.js'
    }
	
    formUpload {
	dependsOn 'jqueryBase'
	resource url:'js/divmodal.js'

    }

    availability {
        dependsOn 'jqueryBase, datePicker'
        resource url:'js/availability.js'
    }

    enableDisableEdit {
        dependsOn 'jqueryBase'
        resource url:'js/enableDisableEdit.js'
    }

    registeredApplication {
        dependsOn 'jqueryBase,jquerySettings'
        resource url:'js/registeredApp.js'
    }
}