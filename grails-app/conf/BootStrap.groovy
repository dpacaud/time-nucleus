import time.nucleus.hotels.Chain
import time.nucleus.hotels.Hotel
import time.nucleus.hotels.Photo
import time.nucleus.hotels.Room
import groovy.time.TimeCategory
import time.nucleus.security.Role
import time.nucleus.security.UserRole
import time.nucleus.security.User
import time.nucleus.hotels.HotelType
import time.nucleus.hotels.Ambiance
import time.nucleus.hotels.Filter
import time.nucleus.api.RegisteredApplication

class BootStrap {

    def availabilityService
    def grailsApplication
    def geocodeService

    def init = { servletContext ->
        //injecting grailsApplication into domains
        for (dc in grailsApplication.domainClasses) {
            dc.clazz.metaClass.getGrailsApplication = { -> grailsApplication }
            dc.clazz.metaClass.static.getGrailsApplication = { -> grailsApplication }
        }
        registerJsonConverters()

        createFolders()

        createHotelTypes()
        createChains()
        createAmbiances()
        createFilters()

        createHotels()

        createRooms()

        createRoles()
        createUsers()

        addUsersToHotels()
        createAvailabilities()
        createRegisteredApplications()


    }

    def registerJsonConverters = {
        grails.converters.JSON.registerObjectMarshaller(Hotel, Hotel.toJson)
        grails.converters.JSON.registerObjectMarshaller(HotelType, HotelType.toJson)
        grails.converters.JSON.registerObjectMarshaller(Photo, Photo.toJson)
    }

    def createFolders = {
        def imgFolder = new File(grailsApplication.config.timeNucleus.photos.rootPath)
        if (!imgFolder.exists()) {
            imgFolder.mkdir()
        }
    }


    def createAmbiances = {
        def monAmbiance
        monAmbiance = Ambiance.findByName("Belle époque")
        if(!monAmbiance){
            new Ambiance(name:"Belle époque",description: "this is a description").save(flush: true)
        }
        monAmbiance = Ambiance.findByName("Charme et confort")
        if(!monAmbiance){
            new Ambiance(name:"Charme et confort",description: "this is a description").save(flush: true)
        }
        monAmbiance = Ambiance.findByName("Design chic")
        if(!monAmbiance){
            new Ambiance(name:"Design chic",description: "this is a description").save(flush: true)
        }
        monAmbiance = Ambiance.findByName("Insolite")
        if(!monAmbiance){
            new Ambiance(name:"Insolite",description: "this is a description").save(flush: true)
        }
        monAmbiance = Ambiance.findByName("Intimiste")
        if(!monAmbiance){
            new Ambiance(name:"Intimiste",description: "this is a description").save(flush: true)
        }
    }

    def createFilters = {
        def monFilter
        monFilter = Filter.findByName("Discrétion")
        if(!monFilter){
            new Filter(name:"Discrétion",description: "this is a description").save(flush: true)
        }
        monFilter = Filter.findByName("Grand luxe")
        if(!monFilter){
            new Filter(name:"Grand luxe",description: "this is a description").save(flush: true)
        }
        monFilter = Filter.findByName("Prix modéré")
        if(!monFilter){
            new Filter(name:"Prix modéré",description: "this is a description").save(flush: true)
        }
        monFilter = Filter.findByName("Facile d'accès")
        if(!monFilter){
            new Filter(name:"Facile d'accès",description: "this is a description").save(flush: true)
        }
        monFilter = Filter.findByName("Paiement cash")
        if(!monFilter){
            new Filter(name:"Paiement cash",description: "this is a description").save(flush: true)
        }
    }

    def createHotelTypes = {
        def myHotelType
        myHotelType = HotelType.findByName('Hôtel')
        if(!myHotelType){
            new HotelType(name:'Hôtel').save(flush: true)
        }
        myHotelType = HotelType.findByName("Chambres d'hôte")
        if(!myHotelType){
            new HotelType(name:"Chambres d'hôte").save(flush: true)
        }
    }
    def createChains = {
        def maChain
        maChain = Chain.findByName("MaChaine")
        if(!maChain){
            new Chain(name:"MaChaine").save(flush: true)
        }
    }

    def createHotels = {
        def random = new Random()
        def filterCount = Filter.count()
        def monHotel
        def resMap
        monHotel = Hotel.findByName("Mon Super Hotel")
        if(!monHotel){
            monHotel = new Hotel(   name: "Mon Super Hotel",
                                    stars: 3,
                                    chain: Chain.findByName("MaChaine"),
                                    cashAccepted: true,
                                    type: HotelType.findByName("Hôtel"),
                                    adressL1:"10 rue des pyrénées",
                                    zipCode: "75020",
                                    city: "Paris",
                                    description: "Mon Super Hotel est un charmant hotel 3 étoiles situé en plein coeur de paris ;)Venez vous y relaxer dans la joie et la bonne hummeur",
                                    telephoneFixe:"0142293933",
                                    telephoneMobile: "0686968686",
                                    fax: "0145677246",
                                    contactEmail:"contact@monHotel.com"
                                )
            resMap = geocodeService.getAdressGPSCoords(monHotel.getCompleteAddress())
            monHotel.lat = resMap.lat
            monHotel.lng = resMap.lng
            if(!monHotel.validate()){
                printErrors(monHotel)
            }
            else {
                monHotel.addToFilters(Filter.get(random.nextInt(filterCount) + 1))
                //geocodeService.getAdressGPSCoords(monHotel.getCompleteAddress())
                monHotel.save(flush: true)
            }
        }
        monHotel = Hotel.findByName("Mon petit hotel")
        if(!monHotel){
            monHotel = new Hotel(   name: "Mon petit hotel",
                    stars: 1,
                    cashAccepted: true,
                    type: HotelType.findByName("Chambres d'hôte"),
                    adressL1:"23 rue ernestine",
                    zipCode: "75018",
                    city: "Paris",
                    telephoneMobile: "0686345675",
                    contactEmail:"guests@tithotel.com"
            )
            resMap = geocodeService.getAdressGPSCoords(monHotel.getCompleteAddress())
            monHotel.lat = resMap.lat
            monHotel.lng = resMap.lng
            if(!monHotel.validate()){
                printErrors(monHotel)
            }
            else {
                monHotel.addToFilters(Filter.get(random.nextInt(filterCount) + 1))
                monHotel.save(flush: true)
            }
        }

        monHotel = Hotel.findByName("Mon hotel de luxe")
        if(!monHotel){
            monHotel = new Hotel(   name: "Mon hotel de luxe",
                    stars: 5,
                    chain: null,
                    cashAccepted: true,
                    type: HotelType.findByName("Hôtel"),
                    adressL1:"10 rue des pyrénées",
                    zipCode: "75010",
                    city: "Paris",
                    telephoneMobile: "0668612449",
                    contactEmail:"serty2@gmail.com"
            )
            resMap = geocodeService.getAdressGPSCoords(monHotel.getCompleteAddress())
            monHotel.lat = resMap.lat
            monHotel.lng = resMap.lng
            if(!monHotel.validate()){
                printErrors(monHotel)
            }
            else {
                monHotel.addToFilters(Filter.get(random.nextInt(filterCount) + 1))
                monHotel.save(flush: true)
            }
        }
        def adress = [:]

        adress.put(1,[adressL1:"55 avenue william Bertrand", zipCode:"17320", city:"Marennes"])
        adress.put(2,[adressL1:"9 rue Félix Faure", zipCode:"75015", city:"Paris"])
        adress.put(3,[adressL1:"14 rue rottembourg", zipCode:"75012", city:"Paris"])
        adress.put(4,[adressL1:"189 boulevard voltaire", zipCode:"75011", city:"Paris"])
        adress.put(5,[adressL1:"10 rue du cardinal lemoine", zipCode:"75005", city:"Paris"])
        adress.put(6,[adressL1:"150 rue de charenton", zipCode:"75012", city:"Paris"])
        (1..6).each {
            //println it
            monHotel = Hotel.findByName("Mon hotel de luxe${it}")
            if(!monHotel){
                //println "Creating Hotel ${it}"
                monHotel = new Hotel(   name: "Mon hotel de luxe${it}",
                        stars: 5,
                        chain: null,
                        cashAccepted: true,
                        type: HotelType.findByName("Hôtel"),
                        adressL1:adress[it].adressL1,
                        zipCode: adress[it].zipCode,
                        city: adress[it].city,
                        telephoneMobile: "0668612449",
                        contactEmail:"serty2@gmail.com"
                )
                resMap = geocodeService.getAdressGPSCoords(monHotel.getCompleteAddress())
                monHotel.lat = resMap.lat
                monHotel.lng = resMap.lng
                if(!monHotel.validate()){
                    printErrors(monHotel)
                }
                else {
                //    println "Saving Hotel ${it}"
                    monHotel.addToFilters(Filter.get(random.nextInt(filterCount) + 1))
                    monHotel.save(flush: true)
                }
            }

        }
    }



    def createRooms = {
        def maChambre = Room.findByName("Chambre double")
        if(!maChambre){
            maChambre = new Room(
                    name: "Chambre double",
                    description: "Ceci est une description succinte de la chambre. orem ipsum dolor sit amet, consectetur adipiscing elit. Integer tincidunt vulputate sem, in dignissim nibh auctor id. Vestibulum at nisi",
                    nbAcceptedPax: 2,
                    hotel:Hotel.findByName("Mon Super Hotel"),
                    ambiance:Ambiance.findByName("Charme et confort")
            )
            if(!maChambre.validate()){
                printErrors(maChambre)
            }
            else{
                maChambre.save(flush: true)
            }
        }
        maChambre = Room.findByName("Suite Royale")
        if(!maChambre){
            maChambre = new Room(
                    name: "Suite Royale",
                    description: "Ceci est une description succinte de la suite. orem ipsum dolor sit amet, consectetur adipiscing elit.",
                    nbAcceptedPax: 2,
                    hotel:Hotel.findByName("Mon Super Hotel"),
                    ambiance:Ambiance.findByName("Charme et confort")
            )
            if(!maChambre.validate()){
                printErrors(maChambre)
            }
            else
                maChambre.save(flush: true)
        }

        maChambre = Room.findByNameAndHotel("Chambre Luxe", Hotel.findByName("Mon hotel de luxe"))
        if(!maChambre){
            maChambre = new Room(
                    name: "Chambre Luxe",
                    description: "Ceci est une description succinte de la la chambre de luxe. orem ipsum dolor sit amet, consectetur adipiscing elit.",
					nbAcceptedPax: 2,
                    hotel:Hotel.findByName("Mon hotel de luxe"),
                    ambiance:Ambiance.findByName("Charme et confort")
            )
            if(!maChambre.validate()){
                printErrors(maChambre)
            }
            else
                maChambre.save(flush: true)
        }


    }
    def createRoles = {
        new Role(authority: 'ROLE_ADMIN').save(flush: true)
        new Role(authority: 'ROLE_HOTEL').save(flush: true)
        new Role(authority: 'ROLE_CONTENT_EDITOR').save(flush: true)
    }

    def createUsers = {

        def hotelUser = User.findByUsername("hotel")

        if(!hotelUser) {
            hotelUser = new User(   username: 'hotel',
                    enabled: true,
                    password: 'hotel',
                    lastName:'Convenant',
                    firstName: 'Jean-Claude',
                    email: 'jc.convenant@yopmail.com'
            )
            if(!hotelUser.validate()){
                hotelUser.errors.each {
                    println it
                }
            }
            else{
                hotelUser.save(flush: true)
                UserRole.create hotelUser, Role.findByAuthority('ROLE_HOTEL'), true
            }
        }

        def noHotelUser = User.findByUsername("noHotel")

        if(!noHotelUser) {
            noHotelUser = new User( username: 'noHotel',
                    enabled: true,
                    password: 'noHotel',
                    lastName:'Duss',
                    firstName: 'Jean-Claude',
                    email: 'jc.duss@yopmail.com',
            )
            if(!noHotelUser.validate()){
                noHotelUser.errors.each {
                    println it
                }
            }
            else{
                noHotelUser.save(flush: true)
                UserRole.create noHotelUser, Role.findByAuthority('ROLE_HOTEL'), true
            }
        }

        def contentEditor = User.findByUsername("content")

        if(!contentEditor) {
            contentEditor = new User(username: 'content', enabled: true, password: 'content',lastName: "Editor", firstName: "Content", email: "damien.pacaud@gmail.com")
            if(!contentEditor.validate()){
                contentEditor.errors.each {
                    println it
                }
            }
            else{
                contentEditor.save(flush: true)
                UserRole.create contentEditor,Role.findByAuthority('ROLE_CONTENT_EDITOR'), true
            }
        }

        def adminUser = User.findByUsername("admin")
        if(!adminUser) {
            adminUser = new User(username: 'admin', enabled: true, password: 'admin',lastName: "Pacaud", firstName: "Damien", email: "damien.pacaud@gmail.com")
            if(!adminUser.validate()){
                adminUser.errors.each {
                    println it
                }
            }
            else{
                adminUser.save(flush: true)
                UserRole.create adminUser,Role.findByAuthority('ROLE_ADMIN'), true
            }
        }
    }

    def  createAvailabilities = {
        Date debut = (new Date() - 5 ).clearTime()
        Date fin = (new Date() + 21 ).clearTime()
        Date debutMatin, finMatin, debutAprem, finAprem, debutJournee, finJournee  = null

        println "Date de début automatic allotment : " + debut
        println "Date de fin automatic allotment : " + fin

        def maChambre1 = Room.findByName("Chambre double")
        for(maDate in debut..fin){


            use(TimeCategory){
                debutMatin = maDate + 9.hours + 30.minutes
                finMatin = maDate + 14.hours + 15.minutes
                debutAprem = maDate + 15.hours + 0.minutes
                finAprem = maDate.clearTime() + 18.hours + 15.minutes
                debutJournee = maDate + 9.hours + 30.minutes
                finJournee = maDate + 18.hours + 15.minutes
            }
            availabilityService.createAvailability(maChambre1,[
                                                    startTime:debutMatin,
                                                    endTime: finMatin,
                                                    total:2,
                                                    basePrice:100,
                                                    reducedPrice:45
                                                    ])

            availabilityService.createAvailability(maChambre1,[
                                                    startTime:debutAprem,
                                                    endTime: finAprem,
                                                    total:2,
                                                    basePrice:100,
                                                    reducedPrice:45
            ])

            availabilityService.createAvailability(maChambre1,[
                                                    startTime:debutJournee,
                                                    endTime: finJournee,
                                                    total:2,
                                                    basePrice:100,
                                                    reducedPrice:70
            ])
        }
    }


    def createRegisteredApplications = {
        def registeredApp = RegisteredApplication.findByName("h2j")
        if(!registeredApp){
            registeredApp = new RegisteredApplication(name : "h2j")
            if(!registeredApp.validate()){
                printErrors(registeredApp)
            }
            else {
                registeredApp.save(flush: true)
            }
        }
        registeredApp = RegisteredApplication.findByName("hotelTardifs")
        if(!registeredApp){
            registeredApp = new RegisteredApplication(name : "hotelTardifs")
            if(!registeredApp.validate()){
                printErrors(registeredApp)
            }
            else {
                registeredApp.save(flush: true)
            }
        }

    }

    def addUsersToHotels = {
        Hotel.findAll().each {
            it.addToUsers(User.findByUsername("hotel"))
        }
    }

    def printErrors(obj) {
        if(obj.errors){
            obj.errors.each {
                log.debug( it )
                println it
            }
        }
    }

    def destroy = {
    }
}
