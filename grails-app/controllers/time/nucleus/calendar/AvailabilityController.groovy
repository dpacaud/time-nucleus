package time.nucleus.calendar

import org.springframework.dao.DataIntegrityViolationException
import time.nucleus.hotels.Hotel
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import time.nucleus.hotels.Room
import groovy.time.TimeCategory
import grails.plugins.springsecurity.Secured

@Secured(["ROLE_HOTEL"])
class AvailabilityController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def springSecurityService
    def hotelService
    def AvailabilityService


    def index() {
        // This is necessary to clean the flash.message in case the previous request did have an error message
        //flash.message = ""

        def currentHotel = Hotel.get(params.hotel)
        def selectedRoom = null
        if (!currentHotel) {
            flash.message = "Une erreur est survenue lors de la recherche de l'hôtel, veuillez ré-essayer svp"
            redirect(controller: "home", action: "index")
            return
        }
        def currentUser = springSecurityService.getCurrentUser()

        if(hotelService.ishotelAvailableForCurrentUser(currentHotel)) {
            if(params.selectedRoom){
                selectedRoom = currentHotel.rooms.find(){it == Room.get(params.selectedRoom)}
                if(params.startDate && params.endDate && params.heureDebut && params.minDebut && params.heureFin && params.minFin && params.nbPax && params.basePrice && params.reducedPrice) {

                }
            }
            if(!selectedRoom) {
                if(currentHotel.rooms.size() > 0 ){
                    selectedRoom = currentHotel.rooms.sort{it.name}.first()
                }
                else {
                    flash.message = "Vous devez ajouter des chambres à votre établissement avant de pouvoir alloter"
                }
            }
        }
        else {
            flash.message = "Vous n'avez pas accès à cet hotel"
            redirect(controller: "home", action: "index")
            return
        }
        [hotelInstance:currentHotel , selectedRoom : selectedRoom  , user:currentUser, activeMenuID:"gestionAllotement${currentHotel.id}"]
    }

    /**
     * Cette action allote une chambre selon des créneaux.
     * @return
     */
    def addAvailabilities() {
        flash.message = ""
        def model = [:]
        def currentHotel = Hotel.get(params.hotel)
        def selectedRoom = null

        if (!currentHotel) {
            flash.message = "Une erreur est survenue lors de la recherche de l'hôtel, veuillez ré-essayer svp"
            redirect(controller: "home", action: "index")
            return
        }

        def currentUser = springSecurityService.getCurrentUser()

        if(hotelService.ishotelAvailableForCurrentUser(currentHotel)) {
            if( params.selectedRoom ){
                selectedRoom = currentHotel.rooms.find(){it == Room.get(params.selectedRoom)}
                if(params.startDate && params.endDate && params.heureDebut
                && params.minDebut && params.heureFin && params.minFin && params.nbRoom
                && params.basePrice && params.reducedPrice){

                    Date start,end

                    for (maDate in (new Date().parse("dd/MM/yy",params.startDate))..(new Date().parse("dd/MM/yy",params.endDate))) {

                        use(TimeCategory){
                            start =  maDate.clearTime() + Integer.parseInt(params.heureDebut).hours + Integer.parseInt(params.minDebut).minutes
                            end = maDate.clearTime() + Integer.parseInt(params.heureFin).hours + Integer.parseInt(params.minFin).minutes
                        }

                        AvailabilityService.createAvailability(selectedRoom,[
                                startTime:start,
                                endTime: end,
                                total:params.nbRoom,
                                basePrice:params.basePrice,
                                reducedPrice:params.reducedPrice
                        ])

                    }
                    flash.success = "L'allotement a bien été réalisé"
                    redirect (controller: "room", action: "show", id:selectedRoom.id)
                    return
                }
                else{
                    model = params + [hotelInstance:currentHotel , selectedRoom : selectedRoom  , user:currentUser, activeMenuID:"gestionAllotement${currentHotel.id}"]
                    flash.message = "Tous les champs sont obligatoires"
                    render(view:"index", model : model)
                    return
                }
            }
            else{
                flash.message = "Une erreur est survenue lors de la recherche de la chambre"
                redirect(controller: "home", action: "index")
                return
            }
        }
        else {
            flash.message = "Vous n'avez pas accès à cet hotel"
            redirect(controller: "home", action: "index")
            return
        }
    }
}
