package time.nucleus.hotels

import org.springframework.dao.DataIntegrityViolationException
import grails.plugins.springsecurity.Secured
import grails.converters.JSON
import org.springframework.context.i18n.LocaleContextHolder

@Secured(['ROLE_HOTEL'])
class HotelController {

    def springSecurityService
    def messageSource
    def geocodeService
    def hotelService
    def photoService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", updatePublished: "POST", getGPSCoordinates: "POST"]

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [hotelInstanceList: Hotel.list(params), hotelInstanceTotal: Hotel.count(),user:springSecurityService.getCurrentUser(), activeMenuID:"hotelList"]
    }

    def create() {
        [newHotelInstance: new Hotel(params) , user:springSecurityService.getCurrentUser() , activeMenuID:"hotelCreate"]
    }

    def save() {
        def hotelInstance = new Hotel(params)

        if (!hotelInstance.save(flush: true)) {
            def errors = []
            hotelInstance.errors?.allErrors?.each {
                errors.push messageSource.getMessage(it, LocaleContextHolder.getLocale())
            }
            flash.message = "Nous n'avons pas pu mettre à jour l'hotel : "
            flash.errors = errors
            render(view: "create", model: [newHotelInstance: hotelInstance,user:springSecurityService.getCurrentUser() , activeMenuID:"hotelCreate"])
            return
        }
        flash.success = "L'hôtel ${hotelInstance.name} a bien été créé"
        redirect(action: "show", id: hotelInstance.id)
    }

    def show(Long id) {
        def hotelInstance = Hotel.get(id)
        if (!hotelInstance || !hotelService.ishotelAvailableForCurrentUser(hotelInstance)) {
            flash.message = "Nous n'avons pas trouvé l'hôtel demandé, veuillez ré-essayer svp"
            // on redirige vers le home controller
            redirect(controller : "home" , action: "index")
            return
        }

        [hotelInstance : hotelInstance , user : springSecurityService.getCurrentUser() , activeMenuID : "ficheHotel${hotelInstance.id}"]
    }

    def update(Long id) {
        //println "id = ${id} version = ${version}"
        def hotelInstance = Hotel.get(id)
        if (!hotelInstance) {
            flash.message = "Nous n'avons pas trouvé l'hôtel demandé, veuillez ré-essayer svp"
            redirect(controller: "home", action: "index")
            return
        }

        hotelInstance.filters.clear()

        params.filters.each {
            hotelInstance.addToFilters(Filter.findById(it))
        }

        hotelInstance.properties = params

        if (!hotelInstance.save(flush: true)) {
            def errors = []
            hotelInstance.errors?.allErrors?.each {
                errors.push messageSource.getMessage(it, LocaleContextHolder.getLocale())
            }
            flash.message = "Nous n'avons pas pu mettre à jour l'hotel : "
            flash.errors = errors
            //TODO : we should probably render the view with the modified model.
            // problem : if we do this, we get stuck with a flash object that needs 2 requests to disapear.
            // hence, if there is an error, it is propagated to the next user click
            //render(view: "show", model: [hotelInstance: hotelInstance,user:springSecurityService.getCurrentUser(), activeMenuID:"ficheHotel${hotelInstance.id}"])
            //return
        }

    //    flash.message = message(code: 'default.updated.message', args: [message(code: 'hotel.label', default: 'Hotel'), hotelInstance.id])
        redirect(action: "show", id: hotelInstance.id)
    }


    def addPhoto(Long id){
        def hotelInstance = Hotel.get(id)

        if(!hotelInstance || !hotelService.ishotelAvailableForCurrentUser(hotelInstance)) {
            flash.message = "Vous n'avez pas l'autorisation d'ajouter une photo à cet hotel"
            redirect(action: "index", controller: "home")
            return
        }
        if(photoService.uploadSinglePhoto(request,hotelInstance)) {
            //log.info "added a photo"
            redirect(action: "show", id: hotelInstance.id)
        }
        else {
            flash.message = "Un problème est survenu lors de l'upload de la photo.\nVérifiez qu'il s'agit bien d'une photo Jpeg ou PNG"
            redirect(action: "index", controller: "home")
        }
    }
/***********************************
 *          AJAX actions
 * *********************************/

     def updatePublished(Long id,boolean isPublished){
        //println "recieved data : id : ${id} isPublished : ${isPublished}"
        def hotelInstance = Hotel.get(id)
        if (hotelInstance) {
            //Let's verify that the user is allowed to publish or unpublish this hotel
            if(hotelService.ishotelAvailableForCurrentUser(hotelInstance) ){
                //println "hotel found"
                hotelInstance.isPublished = isPublished
                hotelInstance.save(flush: true)
                def publishStatus = isPublished ? "publié" : "dé-publié"
                render status: 200, text:"<b>Succès : </b> Votre hôtel ${hotelInstance.name} a été ${publishStatus}"
            }
            else {
                render status: 403,text:"<b>Error : </b> Vous ne pouvez intervenir que sur les hôtels qui vous sont rattachés"
            }
        }
        else {
            render status: 404 ,text: "<b>Error : </b> Nous n'avons pas trouvé l'hôtel demandé"
        }

    }

    def getGPSCoordinates(){
        def hotelInstance = Hotel.get(params.id)

        def resMap

        if (hotelInstance) {
            //Let's verify that the user is allowed to interract with this hotel
            if(hotelService.ishotelAvailableForCurrentUser(hotelInstance)){

                resMap = geocodeService.getAdressGPSCoords(hotelInstance.getCompleteAddress())

                if(resMap && !resMap.locationType?.equals("APPROXIMATE")){
                    hotelInstance.lat = resMap.lat
                    hotelInstance.lng = resMap.lng
                    hotelInstance.save(flush: true)
                    resMap.message = "Vos coordonnées GPS ont bien été mises à jour"
                    render resMap as JSON
                }
                else {
                    render status: 403,text:"<b>Error : </b> Votre adresse semble erronnée, nous ne pouvons mettre à jour les coordonnées GPS.<br> Vérifiez votre adresse svp"
                }
            }
            else {
                render status: 403,text:"<b>Error : </b> Vous ne pouvez intervenir que sur les hôtels qui vous sont rattachés"
            }
        }
        else {
            render status: 404 ,text: "<b>Error : </b> Nous n'avons pas trouvé l'hôtel demandé"
        }
    }
}
