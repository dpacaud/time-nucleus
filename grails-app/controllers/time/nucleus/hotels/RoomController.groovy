package time.nucleus.hotels

import org.apache.commons.logging.LogFactory
import org.springframework.dao.DataIntegrityViolationException
import time.nucleus.calendar.Availability
import org.apache.commons.io.FileUtils
import grails.plugins.springsecurity.Secured


@Secured("ROLE_HOTEL")
class RoomController {

    def springSecurityService
    def photoService
    def hotelService
    def grailsApplication
 
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def create() {
        if(params.hotel){
            def hotelInstance = Hotel.get(params.hotel)
            [roomInstance: new Room(hotel: hotelInstance), hotelInstance:hotelInstance, user:springSecurityService.getCurrentUser(), activeMenuID:"ajouterChambre${hotelInstance.id}" ]
        }
        else {
            redirect(controller: 'home',action: "index")
        }
    }

    def save() {
        def roomInstance = new Room(params)

        def currentHotel = roomInstance.hotel

        if(!currentHotel || !hotelService.ishotelAvailableForCurrentUser(currentHotel)) {
            flash.message = "Vous n'avez pas l'autorisation d'ajouter une chambre à cet hotel."
            redirect(action: "index", controller: "home")
            return
        }

        if (!roomInstance.save(flush: true)) {
            flash.error = "Une erreur est survenue lors de l'enregistrement de la chambre."
            render(view: "create", model: [roomInstance: roomInstance])
            return
        }

        flash.success = "La chambre a bien été ajoutée"
        redirect(action: "show", id: roomInstance.id)


    }
	
    def show(Long id) {
        def roomInstance = Room.get(id)
        if (!roomInstance) {
            flash.message = "La chambre portant l'id ${id} n'a pas été trouvée"
            redirect(controller:"home", action: "index")
            return
        }

        [roomInstance: roomInstance, user:springSecurityService.getCurrentUser(), activeMenuID:"hotel${roomInstance.hotel?.id}room${roomInstance.id}", hotelInstance:roomInstance.hotel]
    }
	
    def update(Long id) {

        def roomInstance = Room.findById(id)
        if (!roomInstance) {
            flash.message = "La chambre portant l'id ${id} n'a pas été trouvée"
            redirect(action: "list")
            return
        }

        roomInstance.properties = params

        if (!roomInstance.save(flush: true)) {
            render(view: "edit", model: [roomInstance: roomInstance])
            return
        }
        redirect(action: "show", id: roomInstance.id)
    }

    def addPhoto(Long id){
        def roomInstance = Room.get(id)

        def currentHotel = roomInstance?.hotel

        if(!currentHotel || !hotelService.ishotelAvailableForCurrentUser(currentHotel)) {
            flash.message = "Vous n'avez pas l'autorisation d'ajouter une photo à cette chambre"
            redirect(action: "index", controller: "home")
            return
        }

        if(photoService.uploadSinglePhoto(request,roomInstance)) {
            log.info "added a photo"
            redirect(action: "show", id: roomInstance.id)
        }
        else {
            flash.message = "Un problème est survenu lors de l'upload de la photo.\nVérifiez qu'il s'agit bien d'une photo Jpeg ou PNG"
            redirect(action: "index", controller: "home")
        }
    }


    @Secured('ROLE_ADMIN')
    def delete(Long id) {

        def roomInstance = Room.get(id)
        def hotelInstance = roomInstance?.hotel

        if (!roomInstance) {
            flash.message = "La chambre portant l'id ${id} n'a pas été trouvée"
            redirect(action: "list")
            return
        }
        try {
            roomInstance.delete(flush: true)
            flash.succes = "La chambre a bien été supprimée"
            redirect(controller: "hotel", action: "show", id: hotelInstance.id)
        }
        catch (DataIntegrityViolationException e) {
            flash.message = "Exception levée lors de la supression de l'a chambre portant l'id : ${id}"
            log.error e
            redirect(action: "show", id: id)
        }

    }

    /***********************************
     *          AJAX actions
     * **********************************/

    /**
     * Cette méthode récupère les disponibilités d'une chambre pour une période donnée.
     * Elle est apellée par fullCalendar via un appel AJAX
     * Elle retourne un tableau JSON d'évènements pour le calendrier
     */
    def listAvailabilities = {

        def debut =  new Date(Long.parseLong(params.startTimestamp))
        def fin =    new Date(Long.parseLong(params.endTimestamp))

        def results = Availability.where {
            and {
                eq("room.id",Long.parseLong(params.roomID))
                between("startTime",debut,fin)

            }
        } 
        render(contentType: "text/json") {
            array {
                for (r in results.list(sort: "id",order: "desc")) {
                    //Date dateFin = new Date(endTime)
                    availability(id: r.id,
                            title:"${r.startTime.format("HH:mm")}\n - \n ${r.endTime.format("HH:mm")} \n dispo :\n ${r.total}",
                            allDay:false,
                            start: r.startTime.format("yyyy-MM-dd HH:mm"),
                            end:r.endTime.format("yyyy-MM-dd HH:mm"),
                            //TODO : mettre à jour l'URL
                            url:"tat",
                            class:"tips-right",
                            )
                }
            }
        }
    }
}