package time.nucleus.api

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils


@Secured(["ROLE_ADMIN"])
class RegisteredApplicationController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def springSecurityService

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        [registeredApplicationInstanceList: RegisteredApplication.list(params),
         registeredApplicationInstanceTotal: RegisteredApplication.count(),
         user:springSecurityService.getCurrentUser(), activeMenuID:"applicationList"
        ]
    }

    def create() {
        [registeredApplicationInstance: new RegisteredApplication(params), user:springSecurityService.getCurrentUser(), activeMenuID:"applicationCreate"]
    }

    def save() {
        def registeredApplicationInstance = new RegisteredApplication(params)
        if (!registeredApplicationInstance.save(flush: true)) {
            registeredApplicationInstance.errors.each {
                println it
            }
            render(view: "create", model: [registeredApplicationInstance: registeredApplicationInstance])
            return
        }

        flash.success = "L'application ${registeredApplicationInstance.name} a bien été ajoutée"
        redirect(action: "show", id: registeredApplicationInstance.id)
    }

    def show(Long id) {
        def registeredApplicationInstance = RegisteredApplication.get(id)
        if (!registeredApplicationInstance) {
            flash.message = "Nous n'avons pas trouvé cette application"
            redirect(action: "list")
            return
        }

        [registeredApplicationInstance: registeredApplicationInstance , user:springSecurityService.getCurrentUser() , activeMenuID:"application${registeredApplicationInstance.id}"]
    }


    def update(Long id, Long version) {
        def registeredApplicationInstance = RegisteredApplication.get(id)
        if (!registeredApplicationInstance) {
            flash.message = "Nous n'avons pas trouvé cette application"
            redirect(action: "list")
            return
        }

        registeredApplicationInstance.name = params.name

        if (!registeredApplicationInstance.save(flush: true)) {
            flash.message = "Une erreur est survenue"
            render(view: "show", model: [registeredApplicationInstance: registeredApplicationInstance])
            return
        }

        flash.success = "L'application a bien été mise à jour"
        redirect(action: "show", id: registeredApplicationInstance.id)
    }

    def delete(Long id) {
        def registeredApplicationInstance = RegisteredApplication.get(id)
        if (!registeredApplicationInstance) {
            flash.message = "Nous n'avons pas trouvé cette application"
            redirect(action: "list")
            return
        }

        try {
            registeredApplicationInstance.delete(flush: true)
            flash.success = "L'application ${registeredApplicationInstance.name} a bien été supprimée"
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = "Exception levée lors de la supression de l'application ${registeredApplicationInstance.name}"
            redirect(action: "show", id: id)
        }
    }


    /***********************************
     *          AJAX actions
     * *********************************/

    def updateActive(Long id,boolean isActive){
       // println "recieved data : id : ${id} isActive : ${isActive}"
        def registeredApplicationInstance = RegisteredApplication.get(id)
        if (registeredApplicationInstance) {
            //Let's verify that the user is allowed to publish or unpublish this hotel
            if(SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")) {
                //println "hotel found"
                registeredApplicationInstance.active = isActive
                if(registeredApplicationInstance.save(flush: true)){
                    def activeStatus = isActive ? "active" : "non-active"
                    render status: 200, text:"<b>Succès : </b> L'application ${registeredApplicationInstance.name} est désormais ${activeStatus}"
                }
                else {
                    render status: 500, text:"<b>Une erreur est survenue lors de l'enregistrement.</b>"
                }
                //return
            }
            else {
                render status: 403,text:"<b>Error : </b> Cette fonctionnalité est réservée aux administrateurs"
            }
        }
        else {
            render status: 404 ,text: "<b>Error : </b> Nous n'avons pas trouvé l'application demandée"
        }

    }
}
