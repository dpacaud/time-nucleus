package time.nucleus.api

import grails.converters.JSON

class ApiController {
    // TODO
    // Mettre à jour ces méthodes lors de la fin du developpemetn
    static allowedMethods = [hotels: "GET"]
    def hotelService
    /**
     * Cette méthode renvoie les hotels dont les coordonnées GPS sont situées à l'intérieur du rectangle décrit par les
     * deux points : minCoo et maxCoo
     * Cette méthode prend en paramètre minCoo et maxCoo. Chaque paramètre est un couple lat,lng
     */
    def hotels() {
        def minCoo = params.minCoo
        def maxCoo = params.maxCoo

        if (minCoo && maxCoo){
            render  hotelService.getHotelsWithinRect(minCoo,maxCoo) as JSON
        }
    }

}
