package time.nucleus

import grails.plugins.springsecurity.Secured
@Secured(['ROLE_HOTEL'])
class HomeController {
    def availabilityService
    def springSecurityService

    def index() {
        def currentHotel = null
        def currentUser = springSecurityService.getCurrentUser()
        // If the user has at least one hotel
        // We pass it to the view in order to populate
        // the rooms menu
        if (currentUser?.hotels){
            currentHotel = currentUser.hotels.first()
        }
       [activeMenuID:"dashboard", user:springSecurityService.getCurrentUser(), hotelInstance:currentHotel]
    }
}
