package time.nucleus.security

import grails.plugins.springsecurity.Secured
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.dao.DataIntegrityViolationException
import time.nucleus.hotels.Hotel

@Secured("ROLE_ADMIN")
class UserController {
    def springSecurityService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [userInstanceList: User.list(params), userInstanceTotal: User.count(),user:springSecurityService.getCurrentUser(),activeMenuID:"userList"]
    }

    def create() {
        [userInstance: new User([enabled:true]),user:springSecurityService.getCurrentUser(),activeMenuID:"userCreate"]
    }

    def save() {
        //println "enabled : " + params.enabled
        def userInstance = new User(params)
        if (!userInstance.save(flush: true)) {
            userInstance.errors.each {
                println it
            }
            render(view: "create", model: [userInstance: userInstance,user:springSecurityService.getCurrentUser(),activeMenuID:"userCreate"])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def show(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance, user:springSecurityService.getCurrentUser(),activeMenuID :"user" + userInstance.id]
    }

    def edit(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def update(Long id, Long version) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'user.label', default: 'User')] as Object[],
                        "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

        userInstance.properties = params

        UserRole.removeAll(userInstance)
        params.roles?.each {
            def tempRole = Role.get(it)
            if(tempRole) {
                UserRole.create(userInstance, tempRole)
            }
        }

        if (!userInstance.save(flush: true)) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def delete(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        try {
            userInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "show", id: id)
        }
    }

    def removeHotelFromUser() {
        def user = User.findById(params.userID)
        def hotel = Hotel.findById(params.hotelID)

        if(user && hotel && SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")){
            user.removeFromHotels(hotel)
            flash.success = "L'hôtel [ ${hotel.name} ] a été supprimé de la liste des hôtels gérés par cet utilisateur"
            redirect action: "show", id: params.userID
        }
        else{
            flash.error = "Vous n'avez pas les autorisations nécessaires pour cette action"
            redirect(controller :"home", action: "index")
        }
    }

    def addHotelsToUser(){
        def user = User.findById(params.userID)
        def hotel = Hotel.findById(params.hotelID)
        if(user && hotel && SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")){
            user.addToHotels(hotel)
            flash.success = "L'hôtel [ ${hotel.name} ] a été ajouté à la liste des hôtels gérés par cet utilisateur"
            redirect action: "show", id: params.userID
        }
        else {
            flash.message = "Vous n'avez pas les autorisations nécessaires pour cette action"
            redirect(controller :"home", action: "index")
        }
    }
}
