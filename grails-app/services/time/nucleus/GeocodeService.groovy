package time.nucleus

import groovy.json.JsonSlurper
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class GeocodeService {

    /**
     * This method takes a string containing an address
     * and tries to get its corresponding GPS coordinates from
     * google geocoding service.
     *
     * @param query
     * @return
     */
    def getAdressGPSCoords(String query) {
        def config = ConfigurationHolder.config
        String googleURL = config.googleapi.url.json

        def urlJSON = new URL(googleURL + query.replaceAll(" ", "+"))
        def resp = urlJSON.getText()

        def geoCodeResultJSON = new JsonSlurper().parseText(resp)
        def jsonMap = [:]
        jsonMap.query = query
        jsonMap.lat = geoCodeResultJSON.results.geometry.location.lat[0]
        jsonMap.lng = geoCodeResultJSON.results.geometry.location.lng[0]
        jsonMap.address = geoCodeResultJSON.results.formatted_address[0]
        jsonMap.locationType = geoCodeResultJSON.results.geometry.location_type[0]
        jsonMap.partialMatch = geoCodeResultJSON.results.partial_match[0]

        return jsonMap
    }
}
