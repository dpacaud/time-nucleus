package time.nucleus

import org.apache.commons.io.FileUtils
import time.nucleus.hotels.Hotel
import time.nucleus.hotels.Photo
import time.nucleus.hotels.Room

class PhotoService {

    def grailsApplication

    /**
     *
     * @param request
     * @param obj
     * @return
     */
	Boolean uploadSinglePhoto(request, obj) {
		def photoFile = request.getFile("photo")
		if (photoFile && !photoFile.empty){
            def computedName = (photoFile.originalFilename + Calendar.instance.time.time).encodeAsBase64()
            def folder = getFolderRelativePath(obj)
            if (folder) {
                def extension =  getExtension(photoFile)
                if (extension) {
                    def relativePath = getFolderRelativePath(obj) + File.separator + computedName + extension
                    def photo = Photo.create( photoFile, obj, relativePath, computedName)
                    if (photo) {
                        photoFile.transferTo( new File(grailsApplication.config.timeNucleus.photos.rootPath + File.separator + relativePath) )
                        return true
                    }
                }
            }
            return false
		}
	}

    /**
     * This method checks wether the the photo path for an object, be it a hotel or a room, exists
     * creates it if it does not exist and return it as a String
     * @param o the hotel or the room
     */
    String getFolderRelativePath(obj) {
        def relativePath

        if (obj instanceof Hotel){
             relativePath = obj.name.encodeAsBase64()
        }
        else if (obj instanceof Room){
             relativePath = obj.hotel.name.encodeAsBase64() + File.separator + obj.name.encodeAsBase64()
        }
        else {
            log.error "This method only works with rooms and hotels"
            return null
        }
        def absolutePath = grailsApplication.config.timeNucleus.photos.rootPath + File.separator +  relativePath

        def tmpFile = new File(absolutePath)

        // if directories do not exist, let's create them
        if (!tmpFile.exists()){
            tmpFile.mkdirs()
        }

        writeIndexHtml(absolutePath)

        return  relativePath

    }

    def getExtension(photoFile) {
        def extension = null
        switch (photoFile.getContentType()) {
            case 'image/jpeg':
                extension = ".jpg"
                break;
            case 'image/png' :
                extension = ".png"
                break;
        }
        return extension
    }


    def writeIndexHtml(path){
        def indexHtml = new File(path + File.separator + "index.html")
        if (!indexHtml.exists()){
            indexHtml.createNewFile()
            def html = """<html>
                        <head>
                            <title>Acc&egrave;s non autoris&eacute;</title>
                        </head>
                        <body>
                            <h1>Vous essayez d'acc&eacute;der &agrave; une zone non autoris&eacute;</h1>
                        </body>
                      </html>"""
            FileUtils.writeStringToFile(indexHtml, html)
        }
    }


}
