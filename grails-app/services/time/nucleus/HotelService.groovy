package time.nucleus

import time.nucleus.hotels.Hotel
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import time.nucleus.security.User

class HotelService {

    def springSecurityService

    /**
     * This method checks wether the current user is allowed to access the requested hotel
     * @param currentHotel the hotel object
     * @return true if the current user has access to this hotel, false otherwise
     */
    boolean ishotelAvailableForCurrentUser(Hotel currentHotel) {
        def currentUser = springSecurityService.getCurrentUser()
       return currentUser.hotels?.contains(currentHotel) || SpringSecurityUtils.ifAllGranted("ROLE_CONTENT_EDITOR")

    }


    def getHotelsWithinRect(minCoo,maxCoo){
        minCoo = minCoo?.split(',')
        double minLat = Double.parseDouble(minCoo[0])
        double minLng = Double.parseDouble(minCoo[1])
        maxCoo = maxCoo?.split(',')
        double maxLat = Double.parseDouble(maxCoo[0])
        double maxLng = Double.parseDouble(maxCoo[1])

        def c = Hotel.createCriteria()
        def ret = c  {
            between("lat", minLat, maxLat)
            between("lng", minLng, maxLng)
            eq("isPublished", true)
        }
        return ret
    }

}
