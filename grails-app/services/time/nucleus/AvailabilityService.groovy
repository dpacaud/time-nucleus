package time.nucleus

import time.nucleus.hotels.Room
import time.nucleus.calendar.Availability

class AvailabilityService {

    def createAvailability(room,params) {

        def tmpAvail = new Availability(
                                        room: room,
                                        startTime: params.startTime,
                                        endTime: params.endTime,
                                        total: params.total,
                                        basePrice: params.basePrice,
                                        reducedPrice: params.reducedPrice
                                        )
        if(!tmpAvail.validate()) {
            tmpAvail.errors.each {
                println it
            }
        }
        room.addToAvailabilities(tmpAvail).save(flush:true)
    }
}
