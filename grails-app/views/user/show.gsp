<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require modules="enableDisableEdit,formUpload" />
    <title>Utilisateur : ${userInstance?.lastName} ${userInstance?.firstName}</title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2>Page de gestion de l'utlisateur : ${userInstance.username} </h2>
                <p>Vous pouvez mettre à jour certaines des informations ci-dessous</p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
        <g:form name="userForm" method="POST" controller="user" action="update" >
            <g:render template="form" />
        </g:form>
        <g:render template="formDelete" model="['userInstance':userInstance]"/>

        <div id="selectHotels" title="Ajout d'hôtel à l'utilisateur">
            <g:uploadForm name="selectHotelsForm" method="post" controller="user" action="addHotelsToUser" >
                <g:hiddenField name="userID" value="${userInstance?.id}" />

                    <g:select name="hotelID" from="${userInstance.getHotelsNotManagedByUser()}" noSelection="['':'-Choisissez un hôtel-']" optionKey="id" optionValue="name"></g:select>

            </g:uploadForm>
        </div>
    </div>
</div>
</body>

</html>
