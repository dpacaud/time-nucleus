<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <title>Ajout d'un utilisateur</title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2>Ajout d'un utilisateur </h2>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
        <g:form name="userForm" method="POST" controller="user" action="save" >
            <g:render template="form" />
        </g:form>
    </div>
</div>
<r:script>
    // Since we are creating an app, we need to remove the disabled attributes from the form
    $('.st-disable, .cb-enable, .cb-disable').removeAttr("disabled");
    $('.st-disable').removeClass("st-disable");
    $('.selector, .checker').removeClass("disabled");

</r:script>
</body>

</html>
