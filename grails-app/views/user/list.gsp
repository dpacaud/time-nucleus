
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require modules="prettyPhoto,hotels" />
    <title>Liste des utilisateurs</title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2> Liste des utilisateurs</h2>
                <p>Vous pouvez ajouter, éditer ou supprimer des utilisateurs</p></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
        <div class="simplebox grid740" style="z-index: 450; ">
            <div class="titleh" style="z-index: 440; ">

            </div>
            <table id="hotelTable" class="tablesorter">
                <thead>
                <tr>
                    <g:sortableColumn property="username" title="Username" />
                    <g:sortableColumn property="lastName" title="Nom de famille" />
                    <g:sortableColumn property="firstName" title="Prénom" />
                    <g:sortableColumn property="authorities" title="Roles" />
                    <g:sortableColumn property="enabled" title="Actif" />
                    <th></th>
                </tr>
                </thead>

                <tbody>
                <g:each in="${userInstanceList}">
                    <tr>
                        <td>${it.username}</td>
                        <td>${it.lastName}</td>
                        <td>${it.firstName}</td>
                        <td>${it.authorities*.authority}</td>
                        <td>${it.enabled}</td>
                        <td>
                            <g:link controller="user" action="show" id="${it.id}" class="icon-button">
                                <r:img dir="images/icons/button/" file="create.png" width="18" height="18" alt="icon" />
                                <span>Edit</span>
                            </g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <ul class="pagination">
            <g:paginate controller="user" action="list" total="${userInstanceTotal}" />
        </ul>
    </div>
</div>
</body>

</html>