<%@ page import="time.nucleus.security.User" %>
<%@ page import="time.nucleus.security.Role" %>

<g:hiddenField name="id" value="${userInstance?.id}" />
<g:hiddenField name="version" value="${userInstance?.version}" />
<div class="simplebox grid450-left">
    <div class="titleh"><h3>Informations générales</h3></div>
    <div class="body">
        <div class="st-form-line">
            <span class="st-labeltext">username : </span>
            <input name="username" type="text"  disabled="disabled" class="st-forminput  st-disable" id="username" style="width:200px" value="${userInstance.username}" />
            <div class="clear"></div>
        </div>
        <g:if test="${params.action == 'create'}">
            <div class="st-form-line">
                <span class="st-labeltext">password : </span>
                <input name="password" type="password"  disabled="disabled" class="st-forminput  st-disable" id="password" style="width:200px" value="${userInstance.username}" />
                <div class="clear"></div>
            </div>
        </g:if>
        <div class="st-form-line">
            <span class="st-labeltext">Nom de famille : </span>
            <input name="lastName" type="text"  disabled="disabled" class="st-forminput  st-disable" id="lastName" style="width:200px" value="${userInstance.lastName}" />
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Prénom : </span>
            <input name="firstName" type="text"  disabled="disabled" class="st-forminput  st-disable" id="firstName" style="width:200px" value="${userInstance.firstName}" />
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Email : </span>
            <input name="email" type="text"  disabled="disabled" class="st-forminput  st-disable" id="email" style="width:200px" value="${userInstance.email}" />
            <div class="clear"></div>
        </div>
    </div>
</div>

<g:if test="${params.action != 'create'}">
    <div class="simplebox grid270-right">
        <div class="titleh"><h3>Mode édition</h3></div>
        <div class="body">
            <div class="st-form-line">
                <p class="field switch">
                    <label id="editButton" class="cb-enable "><span>Activé</span></label>
                    <label id="disableEditButton" class="cb-disable selected"><span>Désactivé</span></label>
                </p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</g:if>
<div class="simplebox grid270-right">
    <div class="titleh"><h3>Utilisateur actif ?</h3></div>
    <div class="body">
        <div class="st-form-line">
            <p class="field switch">
                <g:if test="${userInstance.enabled}">
                    <g:set var="userEnabled" value="selected"/>
                    <g:set var="userDisabled" value=""/>
                    <g:set var="checked" value='checked="checked"'/>
                </g:if>
                <g:else>
                    <g:set var="userEnabled" value=""/>
                    <g:set var="userDisabled" value="selected"/>
                    <g:set var="checked" value=''/>
                </g:else>
                <label id="enabledButton" disabled="disabled" class="cb-enable ${userEnabled}"><span>Oui</span></label>
                <label id="disabledButton" disabled="disabled" class="cb-disable ${userDisabled}"><span>Non</span></label>
                <input id="enabled" name="enabled"  type="checkbox" class="checkbox" ${checked} style="display: none;"/>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="clear"></div>
<g:if test="${params.action != 'create'}">
<div class="grid740 simplebox">
    <div class="titleh"><h3>Roles</h3>
    </div>
    <div class="body">
        <br/>
        <g:each in="${Role.list()}" >
            <label class="margin-left10">
                <input value="${it.id}" type="checkbox" name="roles" id="role${it.id}" disabled="disabled" class="st-forminput  st-disable uniform" style="opacity: 0;" <% userInstance.getAuthorities()?.size() > 0 ? (userInstance?.getAuthorities()*.id.contains(it.id) ? out << "checked=\"true\"" : null ) : null  %> />
                ${it.authority}
            </label>
        </g:each>
        <br/>
        &nbsp;
        <div class="clear"></div>
    </div>
</div>
</g:if>
<div class="grid740 simplebox">
    <div class="titleh"><h3>Hôtels gérés par cet utilisateur</h3>
        <div class="shortcuts-icons">
            <input type="button" id="addHotel" class="st-button tips-right" original-title="Ajouter un hôtel à cet utilisateur" value="+"/>
        </div>
    </div>
    <div class="body">
        <ul class="list-arrow">

            <g:if test="${ userInstance.hotels?.size() > 0 }">
                <g:render template="hotelList" model="['hotelList':userInstance.hotels.sort(){it.id},'userID':userInstance.id]"/>
            </g:if>
            <g:else>
                Cet utilisateur ne gère aucun hôtel
            </g:else>

        </ul>
    </div>
</div>
<div class="clear"></div>
<div class="grid740 simplebox">
    <div class="body">
        <div class="button-box" style="text-align: center;">
            <input type="submit" disabled="disabled" name="button" id="submitRoomFormbutton" value="Submit" class="st-button st-disable"/>
        </div>
    </div>
</div>
