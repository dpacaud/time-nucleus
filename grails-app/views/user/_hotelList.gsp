<table id="userhHotelList" class="tablesorter">
    <thead>
    <tr>
        <g:sortableColumn property="name" title="Nom de l'hôtel" width="50%"/>
        <g:sortableColumn property="isPublished" title="Publié" />
        <th></th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <g:each in="${hotelList}">
        <tr>
            <td>${it.name}</td>
            <td>${it.isPublished ? "Oui" : "Non"}</td>
            <td align="center">
                <g:link controller="hotel" action="show" id="${it.id}" class="icon-button">
                    <r:img dir="images/icons/button/" file="create.png" width="18" height="18" alt="icon" />
                    <span>Edit</span>
                </g:link>
            </td>
            <td align="center">
                <g:link controller="user" action="removeHotelFromUser" params="[hotelID:it.id,userID:userID]" class="icon-button">
                    <r:img dir="images/icons/button/" file="create.png" width="18" height="18" alt="icon" />
                    <span>Supprimer</span>
                </g:link>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>