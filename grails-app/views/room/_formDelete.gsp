<sec:ifAnyGranted roles="ROLE_ADMIN">
    <g:form action="delete" controller="room" >
        <div class="grid740 simplebox">
            <div class="body">
                <div class="button-box" style="text-align: center;">
                    <input type="hidden" name="id" value="${roomInstance.id}" />
                    <g:submitButton name="deleteButton" class="button-red" value="Supprimer la chambre (action irréversible)" />
                </div>
            </div>
        </div>
    </g:form>
</sec:ifAnyGranted>