﻿<%@ page import="time.nucleus.hotels.Ambiance" %>
<g:hiddenField name="id" value="${roomInstance?.id}" />
<g:hiddenField name="hotel.id" value="${roomInstance?.hotel?.id}"/>
<div class="simplebox grid450-left">
    <div class="titleh"><h3>Informations générales</h3></div>
    <div class="body">
        <div class="st-form-line">
            <span class="st-labeltext">Nom de la chambre : </span>
            <input name="name" type="text"  disabled="disabled" class="st-forminput  st-disable" id="name" style="width:200px" value="${roomInstance.name}" />
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Nombre max de clients : </span>
            <input name="nbAcceptedPax" type="text"  disabled="disabled" class="st-forminput  st-disable" id="nbAcceptedPax" style="width:200px" value="${roomInstance.nbAcceptedPax}" />

            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Description : </span>
            <textarea name="description" disabled="disabled" class="st-forminput st-disable" id="description" style="width:400px;resize: none;"  rows="10" cols="30">${roomInstance.description}</textarea>
            <div class="clear"></div>
        </div>
    </div>
</div>

<g:if test="${params.action != 'create'}">
<div class="simplebox grid270-right">
    <div class="titleh"><h3>Mode édition</h3></div>
    <div class="body">
        <div class="st-form-line">
            <p class="field switch">
                <label id="editButton" class="cb-enable "><span>Activé</span></label>
                <label id="disableEditButton" class="cb-disable selected"><span>Désactivé</span></label>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="grid270-right simplebox">
    <div class="titleh"><h3>Photos</h3>
        <div class="shortcuts-icons">
            <input type="button" id="addPicture" class="st-button tips-right" original-title="Ajouter une photo" value="+"/>
        </div>
    </div>
    <div class="body">
        <div class="imagebox">
            <g:each in="${roomInstance.photos}">
               <a href="${grailsApplication.config.timeNucleus.cdn.baseURL + it.relativePath }" rel="prettyPhoto" title="">
                    <img src="${grailsApplication.config.timeNucleus.cdn.baseURL + it.relativePath }" width="62" height="50" alt="img" class="tips" />
               </a>
	        </g:each>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</g:if>

<div class="clear"></div>
<div class="grid740 simplebox">
    <div class="titleh"><h3>Ambiance</h3>
    </div>
    <div class="body">
        <br/>
        <g:each in="${Ambiance.list()}" >
            <label class="margin-left10">
                        <input value="${it.id}" type="radio" name="ambiance.id" id="ambianceBox${it.id}" disabled="disabled" class="st-forminput  st-disable uniform" style="opacity: 0;" <% roomInstance.ambiance?.id == it.id ? out << "checked=\"true\"" : null  %> />
                ${it.name}
            </label>
        </g:each>
        <br/>
        &nbsp;
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<div class="grid740 simplebox">
    <div class="body">
        <div class="button-box" style="text-align: center;">
            <input type="submit" disabled="disabled" name="button" id="submitRoomFormbutton" value="Submit" class="st-button st-disable"/>
        </div>
    </div>
</div>

