<%@ page import="time.nucleus.hotels.Ambiance; time.nucleus.hotels.Room" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require module="room"/>
    <title>${roomInstance.name}</title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2>Ajout d'une chambre à l'hotel : ${hotelInstance?.name} </h2>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
			<g:uploadForm action="save" controller="room">
					<g:render template="form"/>
			</g:uploadForm>
	</div>
</div>
<r:script>

var ROOM = {
        isEnabled:${params.action == "create"}
    }

</r:script>
</body>
</html>
