<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><g:layoutTitle default="Time-Nucleus"/></title>
        <!-- Reset -->
        <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'reset.css')}" />

        <!--[if IE 7]>	  <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'ie7-style.css')}" />	<![endif]-->
        <r:require modules="application"/>
        <g:layoutHead/>
        <r:layoutResources />
    </head>
	<body>
    <sec:ifLoggedIn>

        <div class="wrapper">
            <div id="header"> <!-- START HEADER -->
                <!-- logo -->
                <div class="logo">
                    <g:link controller="home" action="index" class="none">
                        <r:img dir="images" file="logo_test.png" width="50" alt="logo" />
                    </g:link>
                </div>
                <div id="notifications">
                    <div class="clear"></div>
                </div>
                <!-- quick menu -->
                <div id="quickmenu">
                    <div class="clear"></div>
                </div>

                <div id="profilebox">
                    <a href="#" class="display">
                        <r:img dir="images" file="simple-profile-img.jpg" width="33" height="33" alt="profile" />
                        <b><%=user?.lastName%></b><span><%=user?.firstName%></span>
                    </a>
                    <div class="profilemenu">
                        <ul>
                            <li><a href="#">Account Settings</a></li>
                            <li><g:link controller="logout" action="index">Logout</g:link></li>
                        </ul>
                    </div>
                </div>
                <div class="clear"></div>
            </div> <!-- END HEADER -->
            <div id="main"> <!-- START MAIN -->
                <g:render template="/layouts/sidebar/sidebar" />

                    <g:layoutBody/>
                    <r:layoutResources />
                    <div class="clear"></div>

            </div>
            <div id="footer">
                <div class="left-column">© Copyright 2012 - All rights reserved.</div>
                <div class="right-column">Time-Nucleus</div>
            </div>
        </div>
        <script type='text/javascript'>
            $(window).load(function(){

                $("#${activeMenuID}").addClass("active").parent("ul").css("display","block") ;


            });

        </script>
    </sec:ifLoggedIn>
    <sec:ifNotLoggedIn>
        <g:layoutBody/>
        <r:layoutResources />
    </sec:ifNotLoggedIn>
	</body>
</html>
