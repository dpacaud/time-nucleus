<div class="albox succesbox" style="display: <% flash.success ? out << "block;" : out << "none;" %>">
    <div id="succesContent">
        ${flash.success}
    </div>
    <a href="#" class="close tips" title="close">close</a>
</div>
<div class="albox errorbox" style="display: <% flash.message ? out << "block;" : out << "none;" %>">
    <div id="errorContent">
        ${flash.message}
        <ul>
            <g:each in="${flash.errors}" var="monObj" status="i">
                 <li> - ${monObj}</li>
            </g:each>
        </ul>
    </div>
    <a href="#" class="close tips" title="close">close</a>
</div>
<div class="albox informationbox" style="display: none;">
    <div id="informationContent"></div>
    <a href="#" class="close tips" title="close">close</a>
</div>
<div class="albox warningbox" style="display: <% flash.warning ? out << "block;" : out << "none;" %>">
    <div id="warningContent"> ${flash.warning}</div>
    <a href="#" class="close tips" title="close">close</a>
</div>