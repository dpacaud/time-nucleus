<li id="user${userInstance.id}">
    <g:link controller="user" action="show" id="${userInstance.id}">
        ${userInstance.lastName} ${userInstance.firstName}
    </g:link>
</li>