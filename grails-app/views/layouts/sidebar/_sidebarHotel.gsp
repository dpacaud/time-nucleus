<li class="subtitle" id="hotels${hotel.id}">
    <a class="action tips-right" href="#" original-title="Cliquez ici pour ouvrir le menu de l'établissement">
        <r:img dir="images/icons/sidemenu/" file="copy.png" width="16" height="16" alt="icon" />
        ${hotel.name}
        <r:img dir="images/" file="arrow-down.png" width="7" height="4" alt="arrow" class="arrow" />
    </a>
    <ul class="submenu" style="display: none;">
        <li id="ficheHotel${hotel.id}">
            <g:link controller="hotel" action="show" id="${hotel.id}" class="tips-right" original-title="Editez les informations de l'établissement ${hotel.name}">
                <r:img dir="images/icons/sidemenu/" file="vcard.png" width="16" height="16" alt="icon"/>
                Fiche hôtel
            </g:link>
        </li>
        <li id="ajouterChambre${hotel.id}">
            <g:link controller="room" params="[hotel:hotel.id]"  action="create" class="tips-right" original-title="Cliquez ici pour ajouter une nouvelle chambre à votre établissement" >
                <r:img dir="images/icons/sidemenu/" file="file_add.png" width="16" height="16" alt="icon"/>
                Ajouter une chambre
            </g:link>
        </li>
        <li id="gestionAllotement${hotel.id}">
            <g:link controller="availability" params="[hotel:hotel.id]"  action="index" class="tips-right" original-title="Cliquez ici pour gérer l'allotement de vos chambres" >
                <r:img dir="images/icons/sidemenu/" file="calendar.png" width="16" height="16" alt="icon"/>
                Allotement
            </g:link>
        </li>
        <li>
            <a href="#">
            </a>
        </li>
        <g:each in="${hotel.rooms.sort{it.name}}" >
            <li id="hotel${hotel.id}room${it.id}">
                <g:link controller="room" action="show" id="${it.id}">
                    <r:img dir="images/icons/sidemenu/" file="image.png" width="16" height="16" alt="icon"/>
                    ${it.name}
                </g:link>
            </li>
        </g:each>
    </ul>
</li>