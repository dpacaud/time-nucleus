<!-- START SIDEBAR -->
<div id="sidebar">
    <div id="sidemenu">
        <ul>
            <li id="dashboard">
                <g:link controller="home" action="index">
                    <r:img dir="images/icons/sidemenu/" file="laptop.png" width="16" height="16" alt="icon" />
                    Tableau de bord
                </g:link>
            </li>
            <g:if test="user.hotels" >
                <g:each in="${user?.hotels?.sort{it.id}}" >
                    <g:render template="/layouts/sidebar/sidebarHotel" model="['hotel':it]"/>
                </g:each>
            </g:if>
            <sec:ifAnyGranted roles="ROLE_CONTENT_EDITOR">
                <li class="subtitle">
                    <a class="action tips-right" href="#" original-title="Hotels">
                        <r:img dir="images/icons/sidemenu/" file="copy.png" width="16" height="16" alt="icon" />
                        Hotels
                        <r:img dir="images/" file="arrow-down.png" width="7" height="4" alt="arrow" class="arrow" />
                    </a>
                    <ul class="submenu" style="display: none;">
                        <li id="hotelList">
                            <g:link controller="hotel" action="list">
                            <r:img dir="images/icons/sidemenu/" file="list.png" width="16" height="16" alt="icon" />
                            Liste des Hôtels
                            </g:link>
                        </li>
                        <li id="hotelCreate">
                            <g:link controller="hotel" action="create">
                                <r:img dir="images/icons/sidemenu/" file="file_add.png" width="16" height="16" alt="icon" />
                                Ajouter un Hôtel
                            </g:link>
                        </li>
                    </ul>
                </li>
            </sec:ifAnyGranted>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <li class="subtitle">
                    <a class="action tips-right" href="#" original-title="Applications">
                        <r:img dir="images/icons/sidemenu/" file="copy.png" width="16" height="16" alt="icon" />
                        Applications
                        <r:img dir="images/" file="arrow-down.png" width="7" height="4" alt="arrow" class="arrow" />
                    </a>
                    <ul class="submenu" style="display: none;">
                        <li id="applicationList">
                            <g:link controller="registeredApplication" action="list">
                                <r:img dir="images/icons/sidemenu/" file="list.png" width="16" height="16" alt="icon" />
                                Liste des applications
                            </g:link>
                        </li>
                        <li id="applicationCreate">
                            <g:link controller="registeredApplication" action="create">
                                <r:img dir="images/icons/sidemenu/" file="file_add.png" width="16" height="16" alt="icon" />
                                Ajouter une application
                            </g:link>
                        </li>
                    </ul>
                </li>
                <li class="subtitle">
                    <a class="action tips-right" href="#" original-title="Users">
                        <r:img dir="images/icons/sidemenu/" file="copy.png" width="16" height="16" alt="icon" />
                        Utilisateurs
                        <r:img dir="images/" file="arrow-down.png" width="7" height="4" alt="arrow" class="arrow" />
                    </a>
                    <ul class="submenu" style="display: none;">
                        <li id="userList">
                            <g:link controller="user" action="list">
                                <r:img dir="images/icons/sidemenu/" file="list.png" width="16" height="16" alt="icon" />
                                Liste des utilisateurs
                            </g:link>
                        </li>
                        <li id="userCreate">
                            <g:link controller="user" action="create">
                                <r:img dir="images/icons/sidemenu/" file="file_add.png" width="16" height="16" alt="icon" />
                                Ajouter un utilisateur
                            </g:link>
                        </li>
                    </ul>
                </li>
            </sec:ifAnyGranted>
            <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_CONTENT_EDITOR">
                <g:if test="${registeredApplicationInstance}">
                    <g:render template="/layouts/sidebar/sidebarApplication" model="['registeredApplicationInstance':registeredApplicationInstance]"/>
                </g:if>
                <g:if test="${hotelInstance}">
                    <g:render template="/layouts/sidebar/sidebarHotel" model="['hotel':hotelInstance]"/>
                </g:if>
                <g:if test="${userInstance}">
                    <g:render template="/layouts/sidebar/sidebarUser" model="['userInstance':userInstance]"/>
                </g:if>
            </sec:ifAnyGranted>
        </ul>
    </div>
</div>  <!-- END SIDEBAR -->