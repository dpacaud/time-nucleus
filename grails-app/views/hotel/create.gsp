<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require modules="prettyPhoto,hotels" />
    <title>Ajout d'un hôtel </title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2>Ajout d'un hotel</h2>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
        <g:uploadForm action="save" controller="hotel">
            <g:render template="form" model="[hotelInstance:newHotelInstance]"/>
        </g:uploadForm>
    </div>
</div>
<r:script>
    $('.st-disable, .cb-enable, .cb-disable').removeAttr("disabled");
    $('.st-disable').removeClass("st-disable");
    $('.selector, .checker').removeClass("disabled");
</r:script>
</body>
</html>
