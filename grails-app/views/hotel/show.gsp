﻿
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
	<head>
		<meta name="layout" content="main" />

        <r:require modules="prettyPhoto,hotels,enableDisableEdit,formUpload" />

		<title>${hotelInstance.name}</title>
	</head>
	<body>
        <div id="page">
        <g:render template="/layouts/messageBoxes" />
        <!-- start page title -->
            <div class="page-title">
                <div class="in">
                    <div class="titlebar">	<h2>Page de gestion de votre hôtel : ${hotelInstance.name} </h2>
                        <p>Vous pouvez mettre à jour certaines des informations ci-dessous</p></div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content">
                <g:form name="hotelForm" method="post" controller="hotel" action="update">
                    <g:render template="form" />
                </g:form>
                <!-- this div is hidden and only shows on page when it is called -->
                <div id="uploadPhoto" title="Upload de photos">
                    <g:uploadForm name="pictureForm" method="post" controller="hotel" action="addPhoto" >
                        <g:hiddenField name="id" value="${hotelInstance?.id}" />
                        <div class="grid270 simplebox">
                            <input type="file" name="photo" id="photo"/>
                        </div>
                    </g:uploadForm>
                </div>
            </div>
        </div>
	</body>
</html>