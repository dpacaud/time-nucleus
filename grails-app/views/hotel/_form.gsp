﻿<%@ page import="time.nucleus.hotels.HotelType; time.nucleus.hotels.Chain; time.nucleus.hotels.Filter" %>
<g:hiddenField name="id" value="${hotelInstance?.id}" />
<div class="simplebox grid450-left">
    <div class="titleh"><h3>Informations générales</h3></div>
    <div class="body">
        <div class="st-form-line">
            <span class="st-labeltext">Nom de l'hôtel : </span>
            <input name="name" type="text"  disabled="disabled" class="st-forminput  st-disable" id="name" style="width:200px" value="${hotelInstance.name}" />
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Adresse : </span>
            <input name="adressL1" type="text"  disabled="disabled" class="st-forminput  st-disable" id="adressL1" style="width:200px" value="${hotelInstance.adressL1}"/>
            <span class="st-labeltext"></span>
            <input name="adressL2" type="text"  disabled="disabled" class="st-forminput  st-disable" id="adressL2" style="width:200px" value="${hotelInstance.adressL2}" />
            <span class="st-labeltext"></span>
            <input name="zipCode" type="text"  disabled="disabled" class="st-forminput  st-disable tips-left" id="zipCode" style="width:200px" value="${hotelInstance.zipCode}" title="Code Postal" />
            <span class="st-labeltext"></span>
            <input name="city" type="text"  disabled="disabled" class="st-forminput  st-disable tips-left" id="city" style="width:200px" value="${hotelInstance.city}" title="Ville" />
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Site internet : </span>
            <input name="website" type="text"  disabled="disabled" class="st-forminput  st-disable" id="website" style="width:200px" value="${hotelInstance.website}" />
            <div class="clear"></div>
        </div>
    </div>
</div>

<g:if test="${params.action == "show"}">
<div class="simplebox grid270-right">
    <div class="titleh"><h3>Mode édition</h3></div>
    <div class="body">
        <div class="st-form-line">
            <p class="field switch">
                <label id="editButton" class="cb-enable "><span>Activé</span></label>
                <label id="disableEditButton" class="cb-disable selected"><span>Désactivé</span></label>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="simplebox grid270-right">
    <div class="titleh"><h3>Votre hôtel est : </h3></div>
    <div class="body">
        <div class="st-form-line">
            <p class="field switch">
                <g:if test="${hotelInstance.isPublished}">
                    <g:set var="enabled" value="selected"/>
                    <g:set var="disabled" value=""/>
                </g:if>
                <g:else>
                    <g:set var="enabled" value=""/>
                    <g:set var="disabled" value="selected"/>
                </g:else>
                <label id="publishButton" disabled="disabled" class="cb-enable ${enabled}"><span>Online</span></label>
                <label id="unPublishButton" disabled="disabled" class="cb-disable ${disabled}"><span>Offline</span></label>
                <input id="hotelPublish" type="checkbox" class="checkbox" style="display: none;"/>
                <input id="hotelID" type="hidden" value="${hotelInstance.id}"/>
                <input id="publishUrl" type="hidden" value="${createLink(controller:'hotel', action:'updatePublished')}" />
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>
</g:if>
<div class="simplebox grid450-left">
    <div class="titleh"><h3>Informations légales</h3></div>
    <div class="body">
        <div class="st-form-line">
            <span class="st-labeltext">Raison Sociale : </span>
            <g:if test="${params.action != "show"}">
                <input name="raisonSociale" type="text"  disabled="disabled" class="st-forminput  st-disable" id="raisonSociale" style="width:200px" value="${hotelInstance.raisonSociale}" />
            </g:if>
            <g:else>
                <span>${hotelInstance.raisonSociale}</span>
            </g:else>
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Siret : </span>
            <g:if test="${params.action != "show"}">
                <input name="siret" type="text"  disabled="disabled" class="st-forminput  st-disable" id="siret" style="width:200px" value="${hotelInstance.siret}"/>
            </g:if>
            <g:else>
                <span>${hotelInstance.siret}</span>
            </g:else>
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">TVA : </span>
            <g:if test="${params.action != "show"}">
                <input name="tvaNumber" type="text"  disabled="disabled" class="st-forminput  st-disable" id="tvaNumber" style="width:200px" value="${hotelInstance.tvaNumber}" />
            </g:if>
            <g:else>
                <span>${hotelInstance.tvaNumber}</span>
            </g:else>
            <div class="clear"></div>
        </div>
    </div>
</div>
<g:if test="${params.action == "show"}">
<div class="simplebox grid270-right">
    <div class="titleh"><h3>Coordonnées GPS</h3></div>
    <div class="body">
        <div class="st-form-line">
            <p>
                <strong class="hg-purple">lat : <span id="hotelLat">${hotelInstance.lat}</span></strong>
            </p>
            <br />
            <p>
                <strong class="hg-purple">lng : <span id="hotelLng"> ${hotelInstance.lng}</span></strong>
            </p>
            <br />
            <input id="gpsUrl" type="hidden" value="${createLink(controller:'hotel', action:'getGPSCoordinates')}" />
            <div id="calculCoordGPS" class="button-gray tips-left link" original-title="Calcule les coordonnées GPS de votre hôtel">Calculer les coordonées GPS</div>
        </div>
    </div>
</div>
</g:if>
<div class="clear"></div>

<div class="grid160-left simplebox">
    <div class="titleh"><h3>Téléphone fixe</h3>
        <div class="shortcuts-icons">
            <r:img dir="images/icons/button/" file="phone2.png" width="18" height="18" alt="icon"/>
        </div>
    </div>
    <div class="body">
        <div class="st-form-line">
            <input name="telephoneFixe" type="text"  disabled="disabled" class="st-forminput  st-disable" id="telephoneFixe" style="width:106px" value="${hotelInstance.telephoneFixe}" />
            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="grid160-left simplebox">
    <div class="titleh"><h3>Téléphone Mobile</h3>
        <div class="shortcuts-icons">
            <r:img dir="images/icons/button/" file="phone.png" width="18" height="18" alt="icon"/>
        </div>
    </div>
    <div class="body">
        <div class="st-form-line">
            <input name="telephoneMobile" type="text"  disabled="disabled" class="st-forminput  st-disable" id="telephoneMobile" style="width:106px" value="${hotelInstance.telephoneMobile}" />
            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="grid160-left simplebox">
    <div class="titleh"><h3>Fax</h3>
        <div class="shortcuts-icons">
            <r:img dir="images/icons/button/" file="printer.png" width="18" height="18" alt="icon"/>
        </div>
    </div>
    <div class="body">
        <div class="st-form-line">
            <input name="fax" type="text"  disabled="disabled" class="st-forminput  st-disable" id="fax" style="width:106px" value="${hotelInstance.fax}" />

        </div>
    </div>
</div>

<div class="simplebox grid200-right">
    <div class="titleh"><h3>Email</h3>
        <div class="shortcuts-icons">
            <r:img dir="images/icons/button/" file="mail.png" width="18" height="18" alt="icon"/>
        </div>
    </div>
    <div class="body">
        <div class="st-form-line">
            <input name="contactEmail" type="text"  disabled="disabled" class="st-forminput  st-disable" id="contactEmail" style="width:146px" value="${hotelInstance.contactEmail}" />
            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="simplebox grid740">
    <div class="titleh"><h3>Description</h3></div>
    <div class="body">
        <div class="st-form-line">

            <textarea name="description" disabled="disabled" class="st-forminput st-disable" id="description" style="width:680px;resize: none;"  rows="3" cols="47">${hotelInstance.description}</textarea>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="clear"></div>

<div class="simplebox grid450-left">
    <div class="titleh"><h3>Informations détaillées</h3></div>
    <div class="body">
        <div class="st-form-line">
            <span class="st-labeltext">Nombre d'étoiles : </span>
            <input name="stars" type="text" disabled="disabled" class="st-forminput  st-disable" id="stars" style="width:10px" value="${hotelInstance.stars}" />
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Chaine : </span>
            <g:select name="chain.id" id="chain.id" disabled="disabled" class="uniform st-disable" from="${Chain.list()}" value="${hotelInstance.chain?.id}" optionKey="id" optionValue="name" noSelection="['':'- Choisissez une chaine -']" />
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">Type : </span>
            <g:select name="type.id" id="type.id" disabled="disabled" class="uniform st-disable" from="${HotelType.list()}" value="${hotelInstance.type?.id}" optionKey="id" optionValue="name" noSelection="['':'- Choisissez un type -']" />
        </div>
        <div class="st-form-line tips-bottom"  title="Acceptez-vous le paiement en espèces ? ">
            <span class="st-labeltext">Espèces acceptées ? </span>
            <p class="field switch">
                <g:if test="${hotelInstance.isCashAccepted()}">
                    <g:set var="cashEnabled" value="selected"/>
                    <g:set var="cashDisabled" value=""/>
                    <g:set var="checked" value='checked="checked"'/>
                </g:if>
                <g:else>
                    <g:set var="cashEnabled" value=""/>
                    <g:set var="cashDisabled" value="selected"/>
                    <g:set var="checked" value=''/>
                </g:else>
                <label id="cashAcceptedButton" disabled="disabled" class="cb-enable ${cashEnabled}"><span>Oui</span></label>
                <label id="cashNotAcceptedButton" disabled="disabled" class="cb-disable ${cashDisabled}"><span>Non</span></label>
                <input id="cashAccepted" name="cashAccepted" type="checkbox" class="checkbox" ${checked} style="display: none;"/>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="grid270-right simplebox">
    <div class="titleh"><h3>Photos</h3>
        <div class="shortcuts-icons">
            <input type="button" id="addPicture" class="st-button tips-right" original-title="Ajouter une photo" value="+"/>
        </div>
    </div>
    <div class="body">
        <div class="imagebox">
            <g:each in="${hotelInstance.photos}">
                <a href="${grailsApplication.config.timeNucleus.cdn.baseURL + it.relativePath }" rel="prettyPhoto" title="">
                    <img src="${grailsApplication.config.timeNucleus.cdn.baseURL + it.relativePath }" width="62" height="50" alt="img" class="tips" />
                </a>
            </g:each>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<div class="grid740 simplebox">
    <div class="titleh"><h3>Ambiances</h3>
    </div>
    <div class="body">
        <br/>
        <g:each in="${Filter.list()}" >
            <label class="margin-left10">
                <input value="${it.id}" type="checkbox" name="filters" id="filterBox${it.id}" disabled="disabled" class="st-forminput  st-disable uniform" style="opacity: 0;" <% hotelInstance.filters ? (hotelInstance?.filters*.id.contains(it.id) ? out << "checked=\"true\"" : null ) : null  %> />
                ${it.name}
            </label>
        </g:each>
        <br/>
        &nbsp;
        <div class="clear"></div>
    </div>
</div>

<div class="grid740 simplebox">
    <div class="body">
        <div class="button-box" style="text-align: center;">
            <input type="submit" disabled="disabled" name="button" id="submitHotelFormbutton" value="Submit" class="st-button st-disable"/>
        </div>
    </div>
</div>

