
<%@ page import="time.nucleus.hotels.Hotel" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require modules="prettyPhoto,hotels" />
    <title>Liste des hotels</title>
</head>
<body>
    <div id="page">
        <g:render template="/layouts/messageBoxes" />
        <!-- start page title -->
        <div class="page-title">
            <div class="in">
                <div class="titlebar">	<h2> Liste des hôtels</h2>
                    <p>Vous pouvez ajouter, éditer ou supprimer des hôtels</p></div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="content">
            <div class="simplebox grid740" style="z-index: 450; ">
                <div class="titleh" style="z-index: 440; ">

                </div>
                <table id="hotelTable" class="tablesorter">
                    <thead>
                    <tr>
                        <g:sortableColumn property="name" title="Nom de l'hôtel" />
                        <g:sortableColumn property="dateCreated" title="Date de création" />
                        <g:sortableColumn property="isPublished" title="Publié" />
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    <g:each in="${hotelInstanceList}">
                        <tr>
                            <td>${it.name}</td>
                            <td>${it.dateCreated.format("d-M-yyyy")}</td>
                            <td>${it.isPublished}</td>
                            <td>
                                <g:link controller="hotel" action="show" id="${it.id}" class="icon-button">
                                    <r:img dir="images/icons/button/" file="create.png" width="18" height="18" alt="icon" />
                                    <span>Edit</span>
                                </g:link>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>


            </div>

            <ul class="pagination">
                <g:paginate controller="hotel" action="list" total="${hotelInstanceTotal}" />
            </ul>
        </div>
    </div>
</body>

</html>
