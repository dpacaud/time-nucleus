<html>
<head>
	<meta name='layout' content='main'/>
	<title><g:message code="springSecurity.login.title"/></title>
</head>

<body>

<div class="loginform" id='login'>
    <g:render template="/layouts/messageBoxes" />
    <div class="title"> <r:img dir="images" file="logo_test.png" width="50" alt="logo" /></div>
    <div class="body">
        <form id="loginForm" name="form1" method="post" action="${postUrl}">
            <label class="log-lab">Username</label>
            <input name='j_username' id='username' type="text" class="login-input-user" value="Login" onfocus="clearText(this)"/>
            <label class="log-lab">Password</label>
            <input name='j_password' id='password' type="password" class="login-input-pass" value="Password" onfocus="clearText(this)"/>
            <p id="remember_me_holder">
                <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                <label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
            </p>
            <input type="submit" name="button" id="button" value="Login" class="button"/>
        </form>
    </div>
</div>


<script type='text/javascript'>
	function clearText(element){
        if(element.value == "Login" || element.value == "Password" ){
            element.value = ""
        }
    }
</script>
</body>
</html>
