<!DOCTYPE html>
<html>
<head>
    <title>TN - Tableau de bord</title>
    <meta name="layout" content="main">
    <r:require modules="toggle"/>
</head>
<body>
    <div id="page">
        <g:render template="/layouts/messageBoxes" />
        <!-- start page title -->
        <div class="page-title">
            <div class="in">
                <div class="titlebar">	<h2>DASHBOARD</h2>	<p>This is a quick overview of some features</p></div>

                <div class="shortcuts-icons">
                    <a class="shortcut tips" href="#" title="Refresh"><r:img dir = "images/icons/shortcut/" file="refresh.png" width="25" height="25" alt="icon" /></a>
                    <a class="shortcut tips" href="#" title="Dashboard"><r:img dir = "images/icons/shortcut/" file="dashboard.png" width="25" height="25" alt="icon" /></a>
                    <a class="shortcut tips" href="#" title="Add New Item"><r:img dir = "images/icons/shortcut/" file="plus.png" width="25" height="25" alt="icon" /></a>
                    <a class="shortcut tips" href="#" title="Search on This Page"><r:img dir = "images/icons/shortcut/" file="search.png" width="25" height="25" alt="icon" /></a>
                    <a class="shortcut tips" href="#" title="Dashboard is a quick overview of some features"><r:img dir = "images/icons/shortcut/" file="question.png" width="25" height="25" alt="icon" /></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</body>
</html>
