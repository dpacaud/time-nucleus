
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require modules="prettyPhoto,hotels" />
    <title>Liste des applications</title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2> Liste des applications</h2>
                <p>Vous pouvez ajouter, éditer ou supprimer des applications</p></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
        <div class="simplebox grid740" style="z-index: 450; ">
            <div class="titleh" style="z-index: 440; ">

            </div>
            <table id="hotelTable" class="tablesorter">
                <thead>
                <tr>
                    <g:sortableColumn property="name" title="Nom de l'application" />
                    <g:sortableColumn property="dateCreated" title="Date de création" />
                    <g:sortableColumn property="isActive" title="Actif" />
                    <th></th>
                </tr>
                </thead>

                <tbody>
                <g:each in="${registeredApplicationInstanceList}">
                    <tr>
                        <td>${it.name}</td>
                        <td>${it.dateCreated.format("d-M-yyyy")}</td>
                        <td>${it.active}</td>
                        <td>
                            <g:link controller="registeredApplication" action="show" id="${it.id}" class="icon-button">
                                <r:img dir="images/icons/button/" file="create.png" width="18" height="18" alt="icon" />
                                <span>Edit</span>
                            </g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <ul class="pagination">
            <g:paginate controller="registeredApplication" action="list" total="${registeredApplicationInstanceTotal}" />
        </ul>
    </div>
</div>
</body>

</html>