
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <r:require modules="registeredApplication,enableDisableEdit" />
    <title>Application : ${registeredApplicationInstance.name}</title>
</head>
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2>Page de gestion de l'application : ${registeredApplicationInstance.name} </h2>
                <p>Vous pouvez mettre à jour certaines des informations ci-dessous</p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
        <g:form name="applicationForm" method="POST" controller="registeredApplication" action="update" >
            <g:render template="form" />
        </g:form>
        <g:render template="formDelete" model="['registeredApplicationInstance':registeredApplicationInstance]"/>
    </div>
</div>
</body>

</html>
