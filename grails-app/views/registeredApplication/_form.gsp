<g:hiddenField name="id" value="${registeredApplicationInstance?.id}" />
<div class="simplebox grid450-left">
    <div class="titleh"><h3>Informations générales</h3></div>
    <div class="body">
        <div class="st-form-line">
            <span class="st-labeltext">Nom de l'application : </span>
            <input name="name" type="text"  disabled="disabled" class="st-forminput  st-disable" id="name" style="width:200px" value="${registeredApplicationInstance.name}" />
            <div class="clear"></div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext">clef API : </span>
            <span >${registeredApplicationInstance.apiKey}</span>
            <input name="apiKey" type="hidden" value="${registeredApplicationInstance.apiKey}" />
            <div class="clear"></div>
        </div>
    </div>
</div>
<g:if test="${params.action != 'create'}">
    <div class="simplebox grid270-right">
        <div class="titleh"><h3>Mode édition</h3></div>
        <div class="body">
            <div class="st-form-line">
                <p class="field switch">
                    <label id="editButton" class="cb-enable "><span>Activé</span></label>
                    <label id="disableEditButton" class="cb-disable selected"><span>Désactivé</span></label>
                </p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="simplebox grid270-right">
        <div class="titleh"><h3>L'application est : </h3></div>
        <div class="body">
            <div class="st-form-line">
                <p class="field switch">
                    <g:if test="${registeredApplicationInstance.active}">
                        <g:set var="enabled" value="selected"/>
                        <g:set var="disabled" value=""/>
                    </g:if>
                    <g:else>
                        <g:set var="enabled" value=""/>
                        <g:set var="disabled" value="selected"/>
                    </g:else>
                    <label id="activeButton" disabled="disabled" class="cb-enable ${enabled}"><span>Online</span></label>
                    <label id="desactiveButton" disabled="disabled" class="cb-disable ${disabled}"><span>Offline</span></label>
                    <input id="registeredAppActive" type="checkbox" class="checkbox" style="display: none;"/>
                    <input id="registeredAppID" type="hidden" value="${registeredApplicationInstance.id}"/>
                    <input id="activeUrl" type="hidden" value="${createLink(controller:'registeredApplication', action:'updateActive')}" />
                </p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</g:if>
<div class="clear"></div>
<div class="grid740 simplebox">
    <div class="body">
        <div class="button-box" style="text-align: center;">
            <input type="submit" disabled="disabled" name="button" id="submitRoomFormbutton" value="Submit" class="st-button st-disable"/>
        </div>
    </div>
</div>