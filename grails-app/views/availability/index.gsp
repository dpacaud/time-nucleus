<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <title>Gestion de l'allotement</title>
</head>
<r:require modules="availability" />
<body>
<div id="page">
    <g:render template="/layouts/messageBoxes" />
    <!-- start page title -->
    <div class="page-title">
        <div class="in">
            <div class="titlebar">	<h2>Page de gestion de l'allotement de vos chambres </h2>
                <p>Vous pouvez préciser les disponibilités de vos chambres</p>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="content">
            <g:if test="${hotelInstance?.rooms}" >
                <g:form action="addAvailabilities" method="POST">
                    <g:hiddenField name="hotel" value="${hotelInstance.id}" />
                    <g:hiddenField name="selectedRoom" value="${selectedRoom.id}" />
                    <div class="grid450-left simplebox">
                        <div class="titleh"><h3>${selectedRoom.name}</h3></div>
                        <div class="body">
                            <div class="st-form-line">
                                <span class="st-labeltext">Date de début</span>
                                <input type="text" name="startDate" id="startDate" class="datepicker-input" style="width:120px;" />
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                            <div class="st-form-line">
                                <span class="st-labeltext">Date de fin</span>
                                <input type="text" name="endDate" id="endDate" class="datepicker-input" style="width:120px;" />
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                            <div class="st-form-line">
                                <span class="st-labeltext">Créneau horraire</span>
                                <g:select name="heureDebut" from="${(0..24)}" value="9" />:<g:select name="minDebut" from="${["00",15,30,45]}" />
                                &agrave;
                                <g:select name="heureFin" from="${0..24}" value="18" />:<g:select name="minFin" from="${["00",15,30,45]}" />
                                <div class="clear"></div>
                            </div>
                            <div class="st-form-line">
                                <span class="st-labeltext">Nombre de chambres</span>
                                <input type="text" name="nbRoom" class="st-forminput" style="width:30px;" value="${params.nbRoom ? params.nbRoom : 1 }"/>
                                <div class="clear"></div>
                            </div>
                            <div class="st-form-line">
                                <span class="st-labeltext">Prix normal</span>
                                <input type="text" name="basePrice" class="st-forminput" style="width:30px;" value="${basePrice ? basePrice : "" }" />
                                <div class="clear"></div>
                            </div>
                            <div class="st-form-line">
                                <span class="st-labeltext">Prix réduit</span>
                                <input type="text" name="reducedPrice" class="st-forminput" style="width:30px;" value="${reducedPrice ? reducedPrice : "" }" />
                                <div class="clear"></div>
                            </div>
                                <div class="button-box" style="text-align: center;">
                                    <input type="submit" name="button" id="submitAvailabilityFormbutton" value="Submit" class="st-button"/>
                                </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </g:form>
                <g:form action="index" method="POST">
                    <g:hiddenField name="hotel" value="${hotelInstance.id}" />
                    <div class="grid270-right simplebox">
                        <div class="titleh"><h3>Choix de la chambre</h3></div>
                        <div class="body" style="text-align: center;">
                            <g:select name="selectedRoom" from="${hotelInstance?.rooms.sort{it.name}}" value="${selectedRoom?.id}" optionKey="id" optionValue="name" class="uniform" onchange="submit();"/>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="grid270-right simplebox">
                        <div class="titleh"><h3>${selectedRoom.name}</h3></div>
                        <div class="body">
                            ${selectedRoom.description}
                        <div class="clear"></div>
                        </div>
                    </div>
                </g:form>
                <div id='calendar'></div>
            </g:if>



    </div>
</div>
<r:script>

var ROOM = {
    calendarUrl : '${g.createLink(controller: "room", action: "listAvailabilities")}',
    id: '${selectedRoom?.id}'
  }

</r:script>
</body>

</html>