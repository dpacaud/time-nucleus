if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

// This method adds an error to a error notification box
// It should be used with an ajax call.
var addErrors = function(retVal){
    $("#errorContent").html(retVal.responseText);
    $(".errorbox").css("display","block");
};

// This method adds a sucess message to a success notification box
// It should be used with an ajax call.
var addSucess = function(retVal) {
    $("#succesContent").html(retVal);
    $(".succesbox").css("display","block");
};

// This method adds a sucess message to a success notification box
// It should be used with an ajax call.
var addWarning = function(retVal) {
    $("#warningContent").html(retVal);
    $(".warningbox").css("display","block");
};

