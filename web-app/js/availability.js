
var loadAvailabilities = function(startDate, endDate, callback){
    //alert(startDate);
    $.ajax({
        url: ROOM.calendarUrl,
        type:"GET",
        data:   {
                    calendarView:$('#calendar').fullCalendar('getView').name,
                    startTimestamp : startDate.getTime(),
                    endTimestamp :  endDate.getTime(),
                    roomID : "" + ROOM.id
                },
        statusCode: {
            404: addErrors,
            403: addErrors,
            200: callback
        }
    });
};

$(document).ready( function(){
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var monthNames =  ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    var monthNamesShort = ['Janv', 'Fev', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Dec'];
    var daysNames =   ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    var daysNamesShort = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
    $("#startDate").datepicker({
        dayNames : daysNames,
        dayNamesShort:daysNamesShort,
        monthNames:monthNames,
        monthNamesShort:monthNamesShort,
        dateFormat:"dd/mm/yy"
    });
    $("#endDate").datepicker({
        dayNames : daysNames,
        dayNamesShort:daysNamesShort,
        monthNames:monthNames,
        monthNamesShort:monthNamesShort,
        dateFormat:"dd/mm/yy"
    });

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        defaultView:'agendaWeek',
        firstHour:9,
        allDaySlot:false,
        slotMinutes:30,
        axisFormat:"HH:mm",
        firstDay:1,
        monthNames:monthNames,
        monthNamesShort:monthNamesShort,
        dayNames:daysNames,
        dayNamesShort:daysNamesShort,
        titleFormat:{
            month: 'MMMM yyyy',                             // September 2009
            week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}", // Sep 7 - 13 2009
            day: 'dddd, d MMM yyyy'                  // Tuesday, 8 Sep 2009
        },
        columnFormat:{
            month: 'ddd',    // Mon
            week: 'ddd d MMM', // Mon 9 oct
            day: 'dddd d MMM'  // Monday 9 oct
        },
        timeFormat:{
          agenda : 'H:mm { - H:mm}',
          month: 'H:mm { - H:mm} \n'
        },
//        eventMouseover : function( event, jsEvent, view ) {
//            alert(event.id);
//        },
        eventSources:[
            {
                events:loadAvailabilities,
                color: "green"
            }
        ]
    });

});