
$(document).ready( function(){
	$( "#uploadPhoto" ).dialog({
		autoOpen: false,
	    height: 300,
        width: 'auto',
	    modal: false,
	    show_title:true,
	    zIndex: 1500,
	    buttons: {
	    	"Confirmer": function() {
	    		submit = true;
	        	document.pictureForm.submit();
	        	$(this).dialog("close");
	            return true;
	    	 },
	         "Annuler": function() {
	         	$(this).dialog("close");
	         }
	    }
	});
	$( "#addPicture" ).click(function() {
	    	$( "#uploadPhoto"  ).dialog( "open" );
	    	return false;
	});

    $( "#selectHotels" ).dialog({
        autoOpen: false,
        height: 150,
        width: 'auto',
        modal: false,
        show_title:true,
        zIndex: 1500,
        buttons: {
            "Confirmer": function() {
                if($("#hotelID").val() !== ""){
                    submit = true;
                    $("#selectHotelsForm").submit();
                }

                $(this).dialog("close");
                return true;
            },
            "Annuler": function() {
                $(this).dialog("close");
            }
        }
    });
    $( "#addHotel" ).click(function() {
        $( "#selectHotels"  ).dialog( "open" );
        return false;
    });


});
