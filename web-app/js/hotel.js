

$('#addInputFileHotel').click(function() {
	if (typeof this.idFile == "undefined")
		this.idFile = 2;
	var idFile= "hotelFile." + this.idFile++;
	var appendInput = "<br /><label class='margin-left10'><input type='file' name='"+ idFile + "'/></label>";
	$('#uploadFormHotel').after(appendInput);
	});


$(document).ready( function(){
    //////////////// Pretty Photo init code ////////////////////////

    $("a[rel^='prettyPhoto']").prettyPhoto({
                                            animation_speed:'fast',
                                            slideshow:1000,
                                            hideflash: true,
                                            show_title:false,
                                            social_tools:false
                                            });
    //////////////// End Pretty Photo init code ////////////////////////

    $("#publishButton, #unPublishButton").click(function(){
        if($(this).attr("disabled") != "disabled"){
            $.ajax({
                url: $('#publishUrl').val(),
                type:"POST",
                data: {id: $('#hotelID').val() , isPublished:$('#hotelPublish').is(':checked')},
                statusCode: {
                    404: addErrors,
                    403: addErrors,
                    200: addSucess
                }
            });
        }
    });

    $("#calculCoordGPS").click(function(){
        if($("#editButton").hasClass("selected")){
            $.ajax({
                url: $('#gpsUrl').val(),
                data: {id: $('#hotelID').val()},
                type:"POST",
                statusCode: {
                    404: addErrors,
                    403: addErrors,
                    200: function(retVal){
                        //alert("success lat : " + retVal.lat + " lng : " + retVal.lng )
                        $("#hotelLat").html(retVal.lat);
                        $("#hotelLng").html(retVal.lng);
                        if(retVal.partialMatch === true){
                            var message = "Vos coordonnées GPS ont été mises à jour mais un élément de votre adresse semble incorrect, ce qui rend la localisation approximative.";
                            message = message + " Veuillez vérifier votre adresse svp.<br> l'adresse approximée est : ";
                            message = message + "<b>" + retVal.address + "</b>"
                            addWarning( message);

                        } else {
                            addSucess(retVal.message);
                        }
                    }
                }
            });
        }
        else {
             addWarning("Veuillez passer en mode édition pour mettre à jour vos coordonnées GPS")
        }
    });

});
