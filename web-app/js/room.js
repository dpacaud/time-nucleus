// Adding the ajax publish / unpublish
$(document).ready( function(){
    //This code is here because all input fields are disabled by default
    // We need to enable them by default when we create a new room....
    if(ROOM.isEnabled) {
        $('.st-disable, .cb-enable, .cb-disable').removeAttr("disabled");
        $('.st-disable').removeClass("st-disable");
        $('.selector, .checker').removeClass("disabled");

    }
    else {
        // On n'afiche les photos qu'après la création de la chambre
        //////////////// Pretty Photo init code ////////////////////////

        $("a[rel^='prettyPhoto']").prettyPhoto({
            animation_speed:'fast',
            slideshow:1000,
            hideflash: true,
            show_title:false,
            social_tools:false
        });
        //////////////// End Pretty Photo init code ////////////////////////




        //////////////// Full Calendar Init Code    ////////////////////////

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: false,
            defaultView:'agendaWeek',
            firstHour:9,
            allDaySlot:false,
            slotMinutes:30,
            axisFormat:"HH:mm",
            firstDay:1,
            monthNames:['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort:['Janv', 'Fev', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Dec'],
            dayNames:['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort:['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            titleFormat:{
                month: 'MMMM yyyy',                             // September 2009
                week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}", // Sep 7 - 13 2009
                day: 'dddd, d MMM yyyy'                  // Tuesday, 8 Sep 2009
            },
            columnFormat:{
                month: 'ddd',    // Mon
                week: 'ddd d MMM', // Mon 9 oct
                day: 'dddd d MMM'  // Monday 9 oct
            },
            timeFormat:{
                agenda : 'H:mm { - H:mm}',
                month: 'H:mm { - H:mm} \n'
            },
            eventSources:[
                {
                    events:loadAvailabilities,
                    color: "green"
                }
            ]
        });
    }
    //////////////// Fin Full Calendar Init Code ////////////////////////
});

$('#addInputFileRoom').click(function() {
	if (typeof this.count == "undefined")
		this.count = 2;
	var idFile= "hotelFile." + this.count++;
	var appendInput = "<br /><label class='margin-left10'><input type='file' name='"+ idFile + "'/></label>";
	$('#uploadFormRoom').after(appendInput);
	});

var loadAvailabilities = function(startDate, endDate, callback){
    //alert(startDate);
    $.ajax({
        url: ROOM.calendarUrl,
        type:"GET",
        data:   {
                    calendarView:$('#calendar').fullCalendar('getView').name,
                    startTimestamp : startDate.getTime(),
                    endTimestamp :  endDate.getTime(),
                    roomID : "" + ROOM.id
                },
        statusCode: {
            404: addErrors,
            403: addErrors,
            200: callback
        }
    });
};
