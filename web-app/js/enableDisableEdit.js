$(document).ready( function(){
    $('#editButton').click(function() {
        $('.st-disable, .cb-enable, .cb-disable').removeAttr("disabled");
        $('.st-disable').removeClass("st-disable");
        $('.selector, .checker').removeClass("disabled");
        // $(this).addClass('selected');

    });

    $('#disableEditButton').click(function() {
        // We do not want to disable the editButton or the disableEditButton
        $(":input, .cb-enable, .cb-disable").not("#editButton, #disableEditButton").attr("disabled","disabled");
        $(":input").addClass("st-disable");
        $('.selector, .checker').addClass("disabled");
    });

});