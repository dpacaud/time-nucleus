$("docmuent").ready(function() {

    $("#activeButton, #desactiveButton").click(function(){
        if($(this).attr("disabled") != "disabled"){
            $.ajax({
                url: $('#activeUrl').val(),
                type:"POST",
                data: {id: $('#registeredAppID').val() , isActive:$('#registeredAppActive').is(':checked')},
                statusCode: {
                    404: addErrors,
                    403: addErrors,
                    200: addSucess
                }
            });
        }
    });

});